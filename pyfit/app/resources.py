'''This file contains GUI elements that are used repeatedly in different places.'''

##### PyFit modules. #####
#- No modules to import.
####### PyGobject ########
import gi
from libs.platform import gtkversion
gi.require_version('Gtk', gtkversion)
from gi.repository import Gtk, Gdk
###### Time and date #####
import datetime as dt
import gettext
######### Others #########
_g = gettext.gettext

class DateSelectorButton(Gtk.ToggleButton):
    '''This button contains shows a label with the current date assigned to it. When it is toggled, an attached calendar 
    is shown below it, that allows the user to select a specific date.'''
    
    def __init__(self, Date=dt.date.today()):
        super().__init__()
        # Create the window that will cointain the calendar.
        self.calwindow = Gtk.Window.new(Gtk.WindowType.POPUP)
        self.calwindow.set_decorated(False)
        self.calwindow.set_resizable(False)
        self.calwindow.set_sensitive(True)
        self.calwindow.stick()
        self.calwindow.vbox = Gtk.VBox(False, 10)
        self.calwindow.add(self.calwindow.vbox)
        # Create the calendar and put it inside the window.
        self.calendar = Gtk.Calendar()
        self.calendar.select_month(Date.year, Date.month-1)
        self.calendar.select_day(Date.day)
        self.calwindow.vbox.pack_start(self.calendar, True, False, 0)
        # Connect the button to the relevant events.
        self.connect("hierarchy-changed", self.on_anchored)
        self.connect("toggled", self.on_toggle)

    #### Action handlers ###
    
    # Connect the button to its toplevel once it is introduced into it.
    def on_anchored(self, widget, previous_toplevel):
        if self.get_toplevel():
            self.get_toplevel().connect("configure-event", self.on_windowconfig)

    # Show the calendar window when the button is toggled.
    def on_toggle(self, button):
        calendarwindow = self.calwindow
        togglebutton = self
        if togglebutton.get_active():
            buttonspan = togglebutton.get_allocation()
            mainwindow = togglebutton.get_toplevel()
            _, windowx, windowy = mainwindow.get_window().get_origin()
            calendarx = windowx + buttonspan.x
            calendary = windowy + buttonspan.y + buttonspan.height
            x, y = self.coordinatecorrection(calendarx, calendary, calendarwindow, togglebutton)
            calendarwindow.move(x, y)
            calendarwindow.show_all()
        else:
            calendarwindow.hide()

    # Move the calendar window together with the main window.
    def on_windowconfig(self, widget, event):
        calendarwindow = self.calwindow
        togglebutton = self
        if calendarwindow.get_mapped():
            buttonspan = togglebutton.get_allocation()
            calendarx = event.x + buttonspan.x
            calendary = event.y + buttonspan.y + buttonspan.height
            x, y = self.coordinatecorrection(calendarx, calendary, calendarwindow, togglebutton)
            calendarwindow.move(x, y)

    #### Auxiliary functions #####

    # This function corrects the position of the calendar window in order to avoid it going off screen.
    def coordinatecorrection(self, x, y, widget, relativewidget):
        correctedy = y
        correctedx = x
        buttonspan = widget.get_allocation()
        screen = Gdk.Screen.get_default()
        screenw = screen.get_width()
        screenh = screen.get_height()
        changex = screenw - (x + buttonspan.width)
        changey = screenh - (y + buttonspan.height)
        if changex < 0:
            correctedx += changex
        if correctedx < 0:
            correctedx = 0
        if changey < 0:
            correctedy = y - buttonspan.height - relativewidget.get_allocation().height
        if correctedy < 0:
            correctedy = 0
        return [correctedx, correctedy]

class TimeSelector(Gtk.VBox):
    '''This combines two vertical spin buttons to let the user choose a time.'''

    def __init__(self, Time=None):
        super().__init__()
        if not Time:
            Time = dt.datetime.now()
        self.Label = Gtk.Label(_g('Time'))
        self.ControlBox = Gtk.Box()
        self.HourControl = Gtk.SpinButton.new_with_range(0,23,1)
        self.HourControl.set_orientation(Gtk.Orientation.VERTICAL)
        self.HourControl.wrap = True
        self.HourControl.snap_to_ticks = True
        self.HourControl.set_value(Time.hour)
        self.Separator = Gtk.Label(':')
        self.MinuteControl = Gtk.SpinButton().new_with_range(0,59,1)
        self.MinuteControl.set_orientation(Gtk.Orientation.VERTICAL)
        self.MinuteControl.wrap = True
        self.MinuteControl.snap_to_ticks = True
        self.MinuteControl.set_value(Time.minute)
        self.pack_start(self.Label, expand=True, fill=True, padding=0)
        self.pack_start(self.ControlBox, expand=True, fill=True, padding=0)
        self.ControlBox.pack_start(self.HourControl, expand=True, fill=True, padding=0)
        self.ControlBox.pack_start(self.Separator, expand=True, fill=True, padding=0)
        self.ControlBox.pack_start(self.MinuteControl, expand=True, fill=True, padding=0)

