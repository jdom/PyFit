'''Here you will find the elements of the GUI that display the user's data, such as
the sleep time or the weight.'''

##### PyFit modules. #####
from libs import data as db
from libs import platform
from libs.utils import attachlocaltimezone
from libs.data import Activity, Data, Activitytypes
from app.resources import TimeSelector, DateSelectorButton
####### PyGobject ########
import gi
from libs.platform import gtkversion
gi.require_version('Gtk', gtkversion)
from gi.repository import Gtk, GdkPixbuf, GLib
####### Matplotlib #######
import matplotlib
from matplotlib.backends.backend_gtk3agg import (FigureCanvasGTK3Agg as
    FigureCanvas)
###### Time and date #####
import datetime as dt
import calendar as cal
import time
######### Maths ##########
from math import floor, ceil
from statistics import mean
######### Others #########
from PIL import Image as PImage
from copy import deepcopy
import gettext
_g = gettext.gettext

#####################
#### Generic view ###
#####################

class View(Gtk.VBox):
    """
    This is a generic element for visualizing data on the GUI. It is
    characterized the following attributes: a starting date, an ending date, a
    a 'datecontrol' dictionary, an editable flag and a datatype flag. The first
    two determine the date ranges from which the displayed data will be fetched.

    The view contains a set of buttons that let the user change those dates and
    displays them. The third attribute determines by which factor will the
    starting and ending dates change when the user interacts with the
    corresponding buttons.

    Finally, the editable flag determines whether the user can edit, add or
    remove information from the database directly from the view. The datatype
    attribute establishes whether the view displays activities or datapoints.
    """

    #### Properties ##############

    datecontrol = { 'forward':      dt.timedelta(days=1),
                    'back':         -dt.timedelta(days=1),
                    'fastforward':  dt.timedelta(days=7),
                    'fastback':     -dt.timedelta(days=7)
                  }
    editable = False
    datatype = Activity

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, value):
        self._date = value
        self.update()

    @property
    def datelabel(self):
        if self.date == dt.date.today():
            label = _g('Today')
        else:
            label = self.date.strftime('%a, %d %b')
        return label

    @property
    def allowedtypes(self):
        return []

    @property
    def newitem(self):
        now = dt.datetime.now()
        dst = time.localtime(now.timestamp()).tm_isdst
        tz = dt.timezone(dt.timedelta(seconds=-time.timezone) + dst * dt.timedelta(seconds=3600))
        time_n = dt.datetime(self.date.year, self.date.month, self.date.day, now.hour, now.minute, now.second, now.microsecond, tzinfo=tz)
        if self.datatype == Activity:
            item = Activity('NewActivity_' + time_n.isoformat(), \
                                Type=Activitytypes[0], \
                                StartTime=time_n, \
                                EndTime=time_n + dt.timedelta(hours=1) )
        elif self.datatype == Data:
            item = Data(Type='Placeholder', Time=time_n, Value=None, DeviceID='PyFit')
        item.isnew = True
        return item

    @property
    def editmode(self):
        if not self.editable:
            return False
        else:
            if hasattr(self, '_editmode'):
                return self._editmode
            else:
                self.editmode = False

    @editmode.setter
    def editmode(self, value):
        if not self.editable:
            raise Exception(_g('The view is not editable.'))
        else:
            if value == True and self.removemode == True:
                self.buttons['Remove'].set_active(False)
            self._editmode = value
            if hasattr(self, 'on_change_editmode'):
                self.on_change_editmode()

    @property
    def removemode(self):
        if not self.editable:
            return False
        else:
            if hasattr(self, '_removemode'):
                return self._removemode
            else:
                self.removemode = False

    @removemode.setter
    def removemode(self, value):
        if not self.editable:
            raise Exception(_g('The view is not editable.'))
        else:
            if value == True and self.editmode == True:
                self.buttons['Edit'].set_active(False)
            self._removemode = value
            if hasattr(self, 'on_change_removemode'):
                self.on_change_removemode()

    #### Initialization ##########

    def __init__(self, date=dt.date.today()):
        self._date = date

        super().__init__(self)
        self.set_homogeneous(False)
        self.set_spacing(10)
        self.buttons = {}
        #- Upper box: contains the date switcher.
        self.upperbox = Gtk.Box()
        buttons =   { name: Gtk.Button() for name in [
                        'FastBack',
                        'Back',
                        'Forward',
                        'FastForward'
                    ] }
        buttons['Date'] = DateSelectorButton(Date=self.date)
        buttons['Date'].calendar.connect('day-selected', self.on_DateButton_dateselected)
        iconsize = 3
        # Create double arrow image.
        images = {  'FastBack': None,
                    'Back': Gtk.Image.new_from_icon_name('gtk-go-back', iconsize),
                    'Date': None,
                    'Forward': Gtk.Image.new_from_icon_name('gtk-go-forward', iconsize),
                    'FastForward': None,
                 }
        # Create FastBack and FastForward icons.
        for icon, coord, name in (('gtk-go-back', 8, 'FastBack'), ('gtk-go-forward', -8, 'FastForward')):
            iconpixbuf = Gtk.Invisible().render_icon_pixbuf(icon, iconsize)
            imgdata = iconpixbuf.get_pixels()
            image = PImage.frombytes('RGBA', (iconpixbuf.get_width(), iconpixbuf.get_height()), imgdata, "raw", 'RGBA', iconpixbuf.get_rowstride())
            image = image.convert('LA').convert('RGBA')
            copy = image.copy()
            image.paste(copy, (coord, 0), copy)
            imgdata = GLib.Bytes.new( image.tobytes() ).get_data()
            width, height = image.size
            iconpixbuf = GdkPixbuf.Pixbuf.new_from_data(imgdata, GdkPixbuf.Colorspace.RGB, True, 8, width, height, width * 4)
            images[name] =  Gtk.Image.new_from_pixbuf(iconpixbuf)
        for name, button in buttons.items():
            if images[name]:
                button.add(images[name])
            button.props.relief = Gtk.ReliefStyle(2)
            button.name = name
            button.connect("clicked", self.on_DateControlButton_clicked)
            self.buttons[name] = button
        buttons['Date'].props.relief = Gtk.ReliefStyle(0)
        self.upperbox.pack_start(buttons['FastBack'], expand=False, fill=False, padding=0)
        self.upperbox.pack_start(buttons['Back'], expand=False, fill=False, padding=0)
        self.upperbox.pack_start(buttons['Date'], expand=True, fill=True, padding=0)
        self.upperbox.pack_start(buttons['Forward'], expand=False, fill=False, padding=0)
        self.upperbox.pack_start(buttons['FastForward'], expand=False, fill=False, padding=0)
        #- Information box,
        self.infobox = Gtk.VBox()
        #- Lower box: contains various controls.
        self.lowerbox = Gtk.Box()
        if self.editable:
            iconsize = 3
            buttons    = {  'Remove': Gtk.ToggleButton(),
                            'Edit': Gtk.ToggleButton(),
                            'Add': Gtk.Button(),
                            }
            images     = {  'Remove': 'gtk-remove',
                            'Edit': 'gtk-edit',
                            'Add': 'gtk-add',
                         }
            for name, button in buttons.items():
                if images[name]:
                    Image = Gtk.Image.new_from_icon_name(images[name], iconsize)
                    button.add(Image)
                button.props.relief = Gtk.ReliefStyle(2)
                self.lowerbox.pack_start(button, expand=True, fill=False, padding=0)
                self.buttons[name] = button
            buttons['Remove'].connect("toggled", self.on_RemoveModeButton_toggled)
            buttons['Edit'].connect("toggled", self.on_EditModeButton_toggled)
            buttons['Add'].connect("clicked", self.on_AddButton_clicked)
        #- Pack the GUI elements into the view.
        self.pack_start(self.upperbox, expand=False, fill=True, padding=0)
        self.pack_start(self.infobox, expand=True, fill=True, padding=0)
        self.pack_start(self.lowerbox, expand=False, fill=True, padding=0)
        #- Initialize view.
        self.update()

    #### Event handlers ##########

    def update(self):
        # Update data.
        if hasattr(self, 'update_data'):
            self.update_data()
        # Update GUI.
        label = self.datelabel
        self.buttons['Date'].set_label(label.capitalize())
        if hasattr(self, 'update_gui'):
            self.update_gui()

    def addedit(self, item=None, overridetypes=None):
        '''Displays a dialog that lets the user edit or add information, and then stores it in the database.'''
        orig = item
        if not item:
            item = self.newitem
        dialog = AddEditDialog(item=item, view=self)
        if overridetypes:
            types = overridetypes
        else:
            types = self.allowedtypes
        if types:
            dialog.types = types
        else:
            dialog.types = []
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            conn = db.create_connection(platform.databasepath)
            if orig:
                orig.deletefromdatabase(conn)
            dialog.item.writetodatabase(conn)
            conn.commit()
            conn.close()
        dialog.destroy()
        if self.datatype == Activity:
            if dialog.item.Type == 'Sleep':
                self.GUI.views['Journal'].update()
        self.update()

    #### Button event handlers ###

    def on_DateControlButton_clicked(self, button):
        factor = self.datecontrol.get(button.name.lower())
        if factor:
            self.date = self.date + factor

    def on_DateButton_dateselected(self, calendar):
        year, month, day = calendar.get_date()
        self.date = dt.date(year, month+1, day)

    def on_RemoveModeButton_toggled(self, button):
        self.removemode = not self.removemode

    def on_EditModeButton_toggled(self, button):
        self.editmode = not self.editmode

    def on_AddButton_clicked(self, button):
        self.addedit(item=None)

#####################
#### Figure view ####
#####################

### Modes for figure views.

class FigureViewMode():
    """
    A figure view mode determines how the graph in a figure view is displayed.
    The point of creating different figure view modes is to display the data
    among different time spans. They can be initialized as two different modes:
    'bars' and 'plot'. The 'bars' plot mode is useful for aggregating data,
    while the 'plot' mode is useful for plotting data points.

    A figure view mode is characterized by the following attributes: a
    'datecontrol' dictionary, which overrides the one provided by the view,
    and a marker size parameter that controls the marker size in 'plot' mode.
    """

    #### Properties ##############

    datecontrol = { 'forward':      dt.timedelta(days=1),
                    'back':         -dt.timedelta(days=1),
                    'fastforward':  dt.timedelta(days=10),
                    'fastback':     -dt.timedelta(days=10)
                  }
    markersize = None

    @property
    def name(self):
        return 'Default'

    @property
    def datelabel(self):
        label = self.startdate.strftime('%a, %d %b') + ' - ' + self.enddate.strftime('%a, %d %b')
        return label

    @property
    def startdate(self):
        return self.view.date

    @property
    def enddate(self):
        return self.view.date

    #### Initialization ##########

    def __init__(self, mode='bars'):
        self.plotmode = mode

    #### Computation of elements to represent, figure limits and ticks.

    def compute_xy(self, agg=None):
        plotmode = self.plotmode
        if plotmode == 'bars':
            if not agg:
                x, y = self.compute_xy_bars()
            elif agg:
                x, y = self.compute_xy_bars_agg(agg=agg)
            return (x, y)
        elif plotmode == 'plot':
            if agg:
                raise Exception(_g('Cannot aggregate data in plot mode.'))
            return (self.view.dates, self.view.data)
        else:
            raise Exception(_g('Plotting mode {} not supported').format(self.plotmode))

    def compute_xy_bars(self):
            '''In this case the data will not be aggregated, so one expects
            just one datapoint for each date.'''
            x = self.view.dates
            y = self.view.data
            return (x, y)

    def compute_xy_bars_agg(self, agg=None):
            '''In this case, the data will be aggregated. Multiple
            datapoints may exist for the same date, and their values will be
            aggregated using the function "agg". The default figure view mode
            employs just daily aggregations.'''
            # Sometimes there will be datetime objects in self.view.dates instead of date objects.
            dates = [ dt.date(dtime.year, dtime.month, dtime.day) for dtime in self.view.dates] 
            data = self.view.data
            uniquedates = sorted(list(set(dates)))
            x = uniquedates
            y = [ [ data[i] for i in range(0,len(data)) if dates[i] == date ] for date in uniquedates ]
            for i in range(0,len(y)):
                if not y[i]:
                    # For a particular day there might be no data.
                    y[i] = None
                else:
                    # When there is data for a day, and is not none,
                    # aggregate it.
                    toagg = [ yi for yi in y[i] if yi ]
                    if not toagg:
                        # This happens when all data points are none for a day.
                        y[i] = None
                    else:
                        y[i] = agg(toagg)
            # This final part removes days whose data is None.
            x = [ x[i] for i in range(0,len(y)) if y[i] ]
            y = [ y[i] for i in range(0,len(y)) if y[i] ]
            return (x, y)

    def compute_xlims(self):
        mindate = min(self.view.dates)
        maxdate = max(self.view.dates)
        x1 = dt.datetime(mindate.year, mindate.month, mindate.day, 0, tzinfo=mindate.tzinfo)
        x2 = dt.datetime(maxdate.year, maxdate.month, maxdate.day, 23, 59, 59, tzinfo=maxdate.tzinfo)
        return [x1, x2]

    #### Event handlers ##########

    def activate(self):
        if self.plotmode == 'bars':
            # TODO: Enable edits in bars mode.
            self.view.lowerbox.remove(self.view.buttons['Remove'])
            self.view.lowerbox.remove(self.view.buttons['Edit'])
        elif self.plotmode == 'plot':
            # The point of this is to reorder the buttons.
            self.view.lowerbox.remove(self.view.buttons['Add'])
            self.view.lowerbox.pack_start(self.view.buttons['Remove'], expand=True, fill=False, padding=0)
            self.view.lowerbox.pack_start(self.view.buttons['Edit'], expand=True, fill=False, padding=0)
            self.view.lowerbox.pack_start(self.view.buttons['Add'], expand=True, fill=False, padding=0)
        self.view.update()

    def deactivate(self):
        # The edit mode must be disabled before switching to a new figure view mode.
        if self.view.buttons.get('Remove'):
            self.view.buttons['Remove'].set_active(False)
        if self.view.buttons.get('Edit'):
            self.view.buttons['Edit'].set_active(False)

class DailyViewMode(FigureViewMode):
    '''This figure view shows a representation of the data for the day. It is
    meant to work in "plot" mode only.'''

    #### Properties ##############

    datecontrol =   {   'forward':      dt.timedelta(days=1),
                        'back':         -dt.timedelta(days=1),
                        'fastforward':  dt.timedelta(days=7),
                        'fastback':     -dt.timedelta(days=7)
                    }
    markersize = 2

    @property
    def name(self):
        return 'Day'

    @property
    def datelabel(self):
        label = self.view.date.strftime('%a, %d %b')
        if self.view.date == dt.date.today():
            label = _g('Today')
        return label

    #### Initialization ##########

    def __init__(self, mode='plot'):
        super().__init__(mode=mode)
        if self.plotmode != 'plot':
            raise Exception(_g('The daily view mode only works in "plot" mode.'))

    #### Computation of elements to represent, figure limits and ticks.

    def compute_xlims(self):
        x1 = attachlocaltimezone(dt.datetime(self.startdate.year, self.startdate.month, self.startdate.day, 0) - dt.timedelta(hours = 1))
        x2 = attachlocaltimezone(dt.datetime(self.enddate.year, self.enddate.month, self.enddate.day, 23) + dt.timedelta(hours=2))
        return [x1, x2]

    def compute_xticks(self):
        if self.plotmode == 'plot':
            date = self.view.date
            x = [ attachlocaltimezone(dt.datetime(date.year, date.month, date.day, 0)) + dt.timedelta(hours=time) for time in [0,4,8,12,16,20,24] ]
            ticks = [ time for time in [0,4,8,12,16,20,24] ]
            return ( x, ticks )

class WeeklyViewMode(FigureViewMode):
    '''This figure view shows a representation of the data for the week.'''

    #### Properties ##############

    datecontrol = { 'forward':      dt.timedelta(days=1),
                    'back':         -dt.timedelta(days=1),
                    'fastforward':  dt.timedelta(days=7),
                    'fastback':     -dt.timedelta(days=7)
                  }

    markersize = 5

    @property
    def name(self):
        return 'Week'

    @property
    def datelabel(self):
        label = self.startdate.strftime('%a, %d %b') + ' - ' + self.enddate.strftime('%a, %d %b')
        if self.enddate == dt.date.today():
            label = _g('This week')
        return label

    @property
    def startdate(self):
        return self.view.date - dt.timedelta(days=6)

    @property
    def enddate(self):
        return self.view.date

    #### Computation of elements to represent, figure limits and ticks.

    def compute_xlims(self):
        mindate = self.startdate
        maxdate = self.enddate
        if self.plotmode == 'bars':
            x1 = attachlocaltimezone(dt.datetime(mindate.year, mindate.month, mindate.day, 0) - dt.timedelta(hours=12))
            x2 = attachlocaltimezone(dt.datetime(maxdate.year, maxdate.month, maxdate.day, 12))
            return [x1, x2]
        elif self.plotmode == 'plot':
            x1 = attachlocaltimezone(dt.datetime(mindate.year, mindate.month, mindate.day, 0))
            x2 = attachlocaltimezone(dt.datetime(maxdate.year, maxdate.month, maxdate.day, 23, 59, 59))
            return [x1, x2]
        else:
            raise Exception(_g('Plotting mode {} not supported').format(self.plotmode))

    def compute_xticks(self):
        x = self.view.x
        mode = self.plotmode
        dates = sorted([ self.view.enddate - dt.timedelta(days=increment) for increment in range(0,(self.view.enddate-self.view.startdate).days+1)])
        if mode == 'bars':
            return ([ date for date in dates ], [ date.strftime('%a').capitalize() for date in dates])
        elif mode == 'plot':
            return ([ dt.datetime(date.year, date.month, date.day, 12) for date in dates], [ date.strftime('%a').capitalize() for date in dates])
        else:
            raise Exception(_g('Plotting mode {} not supported').format(self.plotmode))

class MonthlyViewMode(FigureViewMode):
    '''This figure view shows a representation of the data for the month.'''

    #### Properties ##############

    markersize = 2

    @property
    def datecontrol(self):
        dictionary = {  'forward': dt.timedelta(days=cal.monthrange(self.view.date.year, self.view.date.month)[1] - self.view.date.day + 1),
                        'back': -dt.timedelta(days=self.view.date.day),
        }
        trans = {-1: 'fastback', 1: 'fastforward'}
        for factor in [-1,1]:
            startmonth = (self.view.date.month + 12*factor) % 12
            if startmonth == 0:
                startmonth = 12
            if self.view.date.month + 12*factor  < 1:
                startyear = self.view.date.year - 1
            elif self.view.date.month + 12*factor > 12:
                startyear = self.view.date.year + 1
            else:
                startyear = self.view.date.year
            startday = self.view.date.day
            if startday == 29:
                startday = 28
            dictionary[trans[factor]] = dt.date(startyear, startmonth, startday) - self.view.date
        return dictionary

    @property
    def datelabel(self):
        return self.startdate.strftime('%B').capitalize() + ' ' + self.startdate.strftime('%Y')

    @property
    def name(self):
        return 'Month'

    @property
    def startdate(self):
        return dt.date(self.view.date.year, self.view.date.month, 1)

    @property
    def enddate(self):
        return dt.date(self.view.date.year, self.view.date.month, cal.monthrange(self.view.date.year, self.view.date.month)[-1])

    # Computation of elements to represent, figure limits and ticks.

    def compute_xlims(self):
        mindate = self.startdate
        maxdate = self.enddate
        x1 = dt.datetime(mindate.year, mindate.month, mindate.day, 0) - dt.timedelta(hours=12)
        x2 = dt.datetime(maxdate.year, maxdate.month, maxdate.day, 12)
        if hasattr(mindate, 'tzinfo'):
            x1 = x1.replace(tzinfo=mindate.tzinfo)
        else:
            x1 = attachlocaltimezone(x1)
        if hasattr(maxdate, 'tzinfo'):
            x2 = x2.replace(tzinfo=mindate.tzinfo)
        else:
            x2 = attachlocaltimezone(x2)
        return [x1, x2]

    def compute_xticks(self):
        x = self.view.x
        mode = self.plotmode
        dates = sorted([ self.enddate - dt.timedelta(days=increment) for increment in range(0,(self.enddate-self.startdate).days+1)])
        locs = [ date for date in dates if date.weekday() == 0 ]
        labels = [ date.strftime('%d %b').capitalize() for date in locs]
        return (locs, labels)

class YearlyViewMode(FigureViewMode):
    '''This figure view shows a representation of the data for the year.'''

    #### Properties ##############

    markersize = 0

    @property
    def datecontrol(self):
        day = self.view.date.day
        if day == 29:
            day = 28
        dictionary = {  'forward': dt.date(self.view.date.year + 1, self.view.date.month, day) - self.view.date,
                        'back': dt.date(self.view.date.year - 1, self.view.date.month, day) - self.view.date,
                        'fastforward': dt.date(self.view.date.year + 10, self.view.date.month, day) - self.view.date,
                        'fastback': dt.date(self.view.date.year - 10, self.view.date.month, day) - self.view.date
                     }
        return dictionary

    @property
    def name(self):
        return 'Year'

    @property
    def datelabel(self):
        return self.view.date.strftime('%Y')

    @property
    def startdate(self):
        return dt.date(self.view.date.year, 1, 1)

    @property
    def enddate(self):
        return dt.date(self.view.date.year, 12, 31)

    # Computation of elements to represent, figure limits and ticks.

    def compute_xy_bars(self):
            raise Exception(_g('Plotting mode {} in "bars" mode without data \
            aggregation not supported').format(self.plotmode))

    def compute_xy_bars_agg(self, agg=None):
            '''In this case, the data will be aggregated. Multiple
            datapoints may exist for the same date, and their values will be
            aggregated using the function "agg". The default figureview mode
            employs just daily aggregations.'''
            dates = self.view.dates
            data = self.view.data
            x = list(range(1,13))
            y = [ [ data[index] for index in range(0, len(dates)) if dates[index].month == month and
                    dt.date(dates[index].year, dates[index].month, dates[index].day) < dt.date.today() ] for month in x ]
            for i in range(0,len(y)):
                if not y[i]:
                    # For a particular month there might be no data.
                    y[i] = None
                else:
                    # When there is data for a month, and is not none,
                    # aggregate it.
                    toagg = [ yi for yi in y[i] if yi ]
                    if not toagg:
                        # This happens when all datapoints are none for a month.
                        y[i] = None
                    else:
                        y[i] = agg(toagg)
            # This final part removes months whose data is None.
            x = [ x[i] for i in range(0,len(y)) if y[i] ]
            y = [ y[i] for i in range(0,len(y)) if y[i] ]
            return (x, y)

    def compute_xticks(self):
        months = list(range(1,13))
        if self.plotmode == 'bars':
            locs = [ month for month in months if month % 2 == 1 ]
            labels = [cal.month_abbr[month].capitalize() for month in locs]
        elif self.plotmode == 'plot':
            locs = [ dt.date(self.startdate.year, month, int(round(cal.monthrange(self.startdate.year, month)[-1]/2))) for month in months if month % 2 == 1 ]
            labels = [cal.month_abbr[month.month].capitalize() for month in locs]
        else:
            raise Exception(_g('Plotting mode {} not supported').format(self.plotmode))
        return (locs, labels)

    def compute_xlims(self):
        if self.plotmode == 'bars':
            return [ 0.5, 12.5 ]
        if self.plotmode == 'plot':
            return [ self.startdate, self.enddate]
        else:
            raise Exception(_g('Plotting mode {} not supported').format(self.plotmode))

### The figure view class.

class FigureView(View):
    """
    This is a View item characterized by the fact that it will contain a graphical
    representation inside its infobox, together with a set of buttons to choose how
    the data will be displayed on the graph. It has an attribute "viewmodes" that
    specifies the view modes available for the view.
    """

    #### Properties ##############

    viewmodes = (FigureViewMode(), )
    activityproperty = 'Duration'
    units = _g('u')

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        if hasattr(self, '_mode'):
            self._mode.deactivate()
        self._mode = value
        self._mode.activate()

    #--- The following properties plotmode, startdate, enddate and datecontrol
    #--- just return the corresponding property of the active mode.

    @property
    def datelabel(self):
        return self.mode.datelabel

    @property
    def plotmode(self):
        return self.mode.plotmode

    @property
    def startdate(self):
        return self.mode.startdate

    @property
    def enddate(self):
        return self.mode.enddate

    @property
    def datecontrol(self):
        if hasattr(self.mode, 'datecontrol'):
            return self.mode.datecontrol
        else:
            return self.datecontrol

    #### Initialization ##########

    def __init__(self, date=dt.date.today()):
        super().__init__(date)

        # Create a label showing the average or the range of the amounts.
        self.infolabel = Gtk.Label(_g('No information'))
        self.infolabel.set_halign(Gtk.Align.CENTER)
        self.infobox.pack_start(self.infolabel, expand=False, fill=False, padding=0)

        # Bind modes to view.
        for mode in self.viewmodes:
            mode.view = self
        self.mode = self.viewmodes[0] # Set viewmode 0 as default.

        # Create the mode selector buttons.
        buttons = []
        for mode in self.viewmodes:
            button = Gtk.Button.new_with_label(mode.name)
            button.mode = mode
            buttons.append(button)
            self.buttons[mode.name] = button
        '''Each button is now placed first at the end of the lowerbox of the
        view and then moved to the beginning of the box. To have the buttons
        in the correct order they need to be reversed before being inserted
        and reordered.'''
        buttons.reverse()
        for button in buttons:
            self.lowerbox.pack_start(button, expand=True, fill=False, padding=0)
            self.lowerbox.reorder_child(button, 0)
            button.connect("clicked", self.on_ModeButton_clicked)

        # Generate and show view contents.
        self.update()

    #### Computation of elements to represent, figure limits and ticks.

    def get_datapoints(self, start, end, conn, type=None):
        if self.datatype == Activity:
            return Activity.fetch_by_date(start, end=end, conn=conn, type=type)
        elif self.datatype == Data:
            return Data.fetch_by_date(start, end=end, conn=conn, type=type)
        else:
            raise Exception(_g('Only the datatypes {} are supported.').format('"' + _g('Activity') + '"' + ' ' + _g('and')+ ' ' + '"' + _g('Data') + '"'))

    '''The methods below are just placeholders and/or methods that refer to the
    active mode.'''

    def compute_xy(self):
        return self.mode.compute_xy()

    def compute_xlims(self):
        if hasattr(self.mode, 'compute_xlims'):
            return self.mode.compute_xlims()

    def compute_xticks(self):
        if hasattr(self.mode, 'compute_xticks'):
            xticks = self.mode.compute_xticks()
            return xticks

    def compute_ylims(self):
        return None

    def compute_yticks(self):
        return None

    #### Event handlers ##########

    def update(self):
        # This check prevents an update when the view is not yet fully initialized.
        if hasattr(self, 'mode') and hasattr(self, 'date'):
            super().update()

    def update_data(self):
        conn = db.create_connection(platform.databasepath)
        if not self.allowedtypes:
            self.datapoints = self.get_datapoints(self.startdate, self.enddate, conn, type=None)
        else:
            for type in self.allowedtypes:
                self.datapoints = self.get_datapoints(self.startdate, self.enddate, conn, type=type)
        if self.datatype == Activity:
            self.datapoints = sorted(self.datapoints, key=lambda x: getattr(x, 'EndTime'))
            self.dates = [ dp.EndTime for dp in self.datapoints ]
            self.data  = [ eval('dp.' + self.activityproperty) for dp in self.datapoints]
        elif self.datatype == Data:
            self.datapoints = sorted(self.datapoints, key=lambda x: getattr(x, 'Time'))
            self.dates = [ dp.Time for dp in self.datapoints ]
            self.data  = [ getattr(dp, 'Value') for dp in self.datapoints]
        else:
            raise Exception(_g('Only the datatypes {} are supported.').format('"' + _g('Activity') + '"' + ' ' + _g('and')+ ' ' + '"' + _g('Data') + '"'))
        conn.close()

    def update_infolabel(self, aggmode='mean'):
        x, y = self.compute_xy()
        if y:
            if aggmode == 'mean':
                aggregated = mean(y)
            elif aggmode == 'range':
                aggregated = (min(y), max(y))
            else:
                raise Exception(_g('Could not update the information label: mode {} not supported.').format(aggmode))
        else:
            aggregated = None
        if aggregated:
            if aggmode == 'mean':
                self.infolabel.set_label(_g('{} {} on average').format(str(round(aggregated, 1)), self.units))
            elif aggmode == 'range':
                self.infolabel.set_label(str(round(aggregated[0], 1)) + '-' + str(round(aggregated[1], 1)) + ' ' + self.units)
            else:
                raise Exception(_g('Could not update the information label: mode {} not supported.').format(aggmode))
        else:
            self.infolabel.set_label(_g('No information'))
        return aggregated

    def update_gui(self):
        self.update_infolabel()
        # Reset the whole figure.
        # TODO: Update the figures instead of resetting them completely.
        if hasattr(self, 'figure'):
            if self.figure:
                self.infobox.remove(self.figure.canvas)
                del self.figure
        # Create a new figure.
        self.figure = matplotlib.figure.Figure(tight_layout=True)
        self.figure.subplots(nrows=1, ncols=1)
        self.axes = self.figure.axes[0]
        self.figure.canvas = FigureCanvas(self.figure)
        self.axes.spines['right'].set_visible(False)
        self.axes.spines['left'].set_visible(False)
        self.axes.spines['top'].set_visible(False)
        self.figure.patch.set_alpha(0)
        self.axes.patch.set_alpha(0)
        # Draw the data.
        self.x, self.y = self.compute_xy()
        if self.plotmode == 'bars': # Update a figure figure of bars type.:
            if not hasattr(self, 'compute_bottom'):
                self.bottom = None
            else:
                self.x, self.bottom = self.compute_bottom()
                self.y_orig = self.y
                self.y = [ self.y[i] - self.bottom[i] for i in range(0,len(self.y)) ]
                # TODO: Think of a better way to represent bars without height.
                for i in range(0, len(self.y)): # Fix 0 height.
                    if self.y_orig[i] == self.bottom[i] and self.bottom[i] != 0:
                        self.y[i] += 1
                        self.bottom[i] -= 1
            self.figure.plot = self.axes.bar(self.x, self.y, bottom=self.bottom)
            # TODO: Implement a similar function to the one in the plot mode that
            # lets the user see the actual bottom and/or top value of the bar.
        elif self.plotmode == 'plot':
            style = '-'
            additional = {}
            if self.mode.markersize:
                style += 'o'
                additional['markersize'] = self.mode.markersize
            self.figure.plot = self.axes.plot(self.x, self.y, style, **additional)
            self.axes.annotation = self.axes.annotate("", xy=(0,0), xytext=(-20,20),textcoords="offset points",
                bbox=dict(boxstyle="round", fc="w"),
                arrowprops=dict(arrowstyle="->"))
            self.axes.annotation.set_visible(False)
            # This displays a popup with the actual value of data points.
            self.figure.canvas.mpl_connect("motion_notify_event", self.on_Hover)
            self.figure.canvas.mpl_connect("button_release_event", self.on_Release)
        else:
            raise Exception(_g('Plot mode {} not supported').format(self.mode.plotmode))
        #- X axis
        xlims = self.compute_xlims()
        if xlims:
            self.axes.set_xlim(xlims)
        xticks = self.compute_xticks()
        if xticks:
            locs, labels = xticks
            self.axes.set_xticks(locs)
            self.axes.set_xticklabels(labels)
        #- Y axis
        ylims = self.compute_ylims()
        if ylims:
            self.axes.set_ylim(ylims)
        yticks = self.compute_yticks()
        if yticks:
            locs, labels = yticks
            self.axes.set_yticks(locs)
            self.axes.set_yticklabels(labels)
        self.infobox.pack_start(self.figure.canvas, expand=True, fill=True, padding=0)
        self.figure.canvas.draw()
        self.figure.canvas.show()

    #### Action event handlers ###

    def on_Release(self, event):
        '''This function serves to delete or edit data points when clicked if the respective modes are enabled.'''
        if event.inaxes == self.axes:
            cont, ind = self.figure.plot[0].contains(event)
            if cont:
                ind = ind['ind'][0]
                if self.editable:
                    if self.editmode == True:
                        self.addedit(item=self.datapoints[ind])
                    elif self.removemode == True:
                        conn = db.create_connection(platform.databasepath)
                        self.datapoints[ind].deletefromdatabase(conn)
                        conn.commit()
                        conn.close()
                        self.update()

    def on_Hover(self, event):
        '''This function shows the value of a data point when the user places the mouse on top of it.'''
        if event.inaxes == self.axes:
            cont, ind = self.figure.plot[0].contains(event)
            if cont:
                ind = ind['ind'][0]
                self.update_annotation(ind)
                xlims = self.compute_xlims()
                x = self.axes.annotation.xy[0]
                if type(xlims[0]) == dt.date:
                    lim0 = attachlocaltimezone(dt.datetime(xlims[0].year, xlims[0].month, xlims[0].day, 0, 0))
                    lim1 = attachlocaltimezone(dt.datetime(xlims[1].year, xlims[1].month, xlims[1].day, 23, 59, 59))
                else:
                    lim0 = xlims[0]
                    lim1 = xlims[1]
                if not (x < lim0 or x > lim1):
                    self.axes.annotation.set_visible(True)
                    self.figure.canvas.draw_idle()
            else:
                if self.axes.annotation.get_visible():
                    self.axes.annotation.set_visible(False)
                    self.figure.canvas.draw_idle()

    def on_ModeButton_clicked(self, button):
        self.mode = button.mode

    #### Auxiliary functions #####

    def update_annotation(self, ind):
        x, y = self.figure.plot[0].get_data()
        x = x[ind]; y = y[ind]
        if not hasattr(x, 'tzinfo'):
            x = attachlocaltimezone(x)
        self.axes.annotation.xy = (x, y)
        text = "{}".format(str(round(y, 1)))
        self.axes.annotation.set_text(text)
        self.axes.annotation.get_bbox_patch().set_alpha(0.8)

#####################
#### Actual views ###
#### used in the  ###
#### program      ###
#####################

class JournalView(View):
    """
    This is a figureview object that will display the user's activities for a
    given day.
    """
    #### Properties ##############

    editable = True
    datatype = Activity

    @property
    def allowedtypes(self):
        if not hasattr(self, '_allowedtypes'):
            return Activitytypes
        elif not self._allowedtypes:
            return Activitytypes
        else:
            return self._allowedtypes

    @allowedtypes.setter
    def allowedtypes(self, value):
        self._allowedtypes = value

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, value):
        self._date = value
        self.update()
        # Changes the date of the goals view as well.
        #TODO: What to do when a weekly time frame for the goals view is allowed? Update the goals view to this view's week.
        if self.GUI.views.get('Goals') != None:
            self.GUI.views['Goals'].date = value

    #### Initialization ##########

    def __init__(self, date=dt.date.today()):
        self.activitylist = None
        self._editmode = False
        super().__init__(date=date)

        # Update the gui.
        self.update()

    #### Event handlers ##########

    def update_data(self):
        conn = db.create_connection(platform.databasepath)
        self.activitylist = Activity.fetch_by_date(self.date, conn=conn)
        conn.close()
        self.activitylist = sorted(self.activitylist, key = lambda activity: activity.EndTime)

    def update_gui(self):
        # Clear the view contents.
        for item in self.infobox.get_children():
            self.infobox.remove(item)
        # Generate list of cards
        cardlist = []
        for item in self.activitylist:
            # INFOLABEL: Construct label containing activity information.
            info = Gtk.Label()
            activity = item
            if activity.Distance:
                distancestr = ' - ' + ('%.2f' % round(activity.Distance/1000, 2)).rjust(5) + ' ' + _g('km')
            else:
                distancestr = ''
            if activity.Duration.total_seconds()/3600 < 1:
                timestr = ' ( ' + str(round(activity.Duration.total_seconds()/60)).rjust(2) + ' ' + _g('min') +' )'
            else:
                timestr = ' ( ' + str(round(activity.Duration.total_seconds() // 3600)).rjust(2) + _g('h')
                timestr += str(round(activity.Duration.total_seconds()/60) % 60 ).rjust(2) + ' ' + _g('min') + ' )'
            if activity.Type == 'Sleep':
                datetimestr = activity.StartTime.strftime('%H:%M') + ' - ' + activity.EndTime.strftime('%H:%M')
            else:
                datetimestr = activity.StartTime.strftime('%H:%M')
            info.set_label(activity.Type + '\n' + datetimestr + timestr + distancestr )
            info.set_alignment(0,0)
            info.set_size_request(200,5)
            # ICONBOX: this is the place where the icon is displayed.
            iconbox = Gtk.Box()
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
                filename=platform.get_icon(item.Type),
                width=30,
                height=30,
                preserve_aspect_ratio=True)
            icon = Gtk.Image.new_from_pixbuf(pixbuf)
            iconbox.pack_start(icon, expand=True, fill=True, padding=0)
            iconbox.set_margin_top(7)
            # MAINBOX: this is the activity "card".
            mainbox = Gtk.Box()
            mainbox.pack_start(info, expand=False, fill=True, padding=0)
            mainbox.pack_start(iconbox, expand=False, fill=True, padding=0)
            mainbox.item = item
            cardlist.append(mainbox)
        if cardlist:
            scrollwindow = Gtk.ScrolledWindow()
            self.infobox.pack_start(scrollwindow, expand=True, fill=True, padding=0)
            scrollbox = Gtk.VBox(spacing = 10)
            scrollbox.props.margin_left = 50
            scrollbox.props.margin_right = 50
            scrollwindow.add(scrollbox)
            for box in cardlist:
                box.buttons = {}
                box.set_margin_top(3)
                box.set_margin_bottom(3)
                scrollbox.pack_start(box, expand=False, fill=True, padding=0)
                box.set_halign(Gtk.Align.CENTER)
        if not cardlist:
            box = Gtk.VBox()
            text = Gtk.Label()
            text.set_label(_g('No events for this day.'))
            self.infobox.pack_start(box, expand=True, fill=True, padding=0)
            box.pack_start(text, expand=True, fill=True, padding=0)
        self.infobox.show_all()
        self.show_edit_buttons()

    def on_change_editmode(self):
        self.show_edit_buttons()

    def on_change_removemode(self):
        self.show_edit_buttons()

    #### Button event handlers ###

    def on_Remove_clicked(self, button):
        cardlistbox = button.get_parent()
        card = button.get_child()
        conn = db.create_connection(platform.databasepath)
        card.item.deletefromdatabase(conn)
        conn.commit()
        conn.close()
        cardlistbox.remove(card.get_parent())
        if not cardlistbox.get_children():
            self.update()
        if card.item.Type == 'Sleep':
            self.GUI.views['Sleep'].update()

    def on_Edit_clicked(self, button):
        card = button.get_child()
        if card.item.Type == 'Sleep':
            self.allowedtypes = ['Sleep']
        self.addedit(item=card.item)
        if card.item.Type == 'Sleep':
            self.allowedtypes = None
        if card.item.Type == 'Sleep':
            self.GUI.views['Sleep'].update()

    #### Auxiliary functions #####

    def show_edit_buttons(self):
        if self.activitylist:
            scrollbox = self.infobox.get_children()[0].get_children()[0].get_children()[0]
            cards = self.infobox.get_children()[0].get_children()[0].get_children()[0].get_children()
            if self.editmode or self.removemode:
                for card in cards:
                    button = Gtk.Button()
                    scrollbox.remove(card)
                    card.set_margin_top(0)
                    card.set_margin_bottom(0)
                    button.add(card)
                    button.props.relief = Gtk.ReliefStyle(0)
                    scrollbox.pack_start(button, expand=False, fill=True, padding=0)
                    button.set_halign(Gtk.Align.CENTER)
                    if self.editmode:
                        button.connect("clicked", self.on_Edit_clicked)
                    elif self.removemode:
                        button.connect("clicked", self.on_Remove_clicked)
                    button.show_all()
            elif not self.editmode and not self.removemode:
                if type(cards[0]) == Gtk.Button:
                    for card in cards:
                        cardbox = card.get_child()
                        card.remove(cardbox)
                        scrollbox.remove(card)
                        cardbox.set_margin_top(3)
                        cardbox.set_margin_bottom(3)
                        scrollbox.pack_start(cardbox, expand=False, fill=True, padding=0)
                        cardbox.set_halign(Gtk.Align.CENTER)

class HeartRateView(FigureView):
    """
    This is a figureview object that will display the user's heart rate.
    """

    #### Properties ##############

    viewmodes = (DailyViewMode(mode='plot'), WeeklyViewMode(mode='bars'), MonthlyViewMode(mode='bars'), YearlyViewMode(mode='bars'))
    datatype = Data
    editable = True
    units = _g('bpm')

    @property
    def allowedtypes(self):
        return ['HeartRate']

    @property
    def newitem(self):
        now = dt.datetime.now()
        time = dt.datetime(self.date.year, self.date.month, self.date.day, now.hour, now.minute, now.second, now.microsecond)
        time = attachlocaltimezone(time)
        item = Data(Type='HeartRate', Time=time, Value=80, DeviceID='PyFit')
        return item

    @property
    def average(self):
        x, y = self.compute_xy(agg=None)
        if y:
            average = mean(y)
        else:
            average = None
        return average

    @property
    def range(self):
        x, y = self.compute_xy(agg=None)
        if y:
            range = (min(y), max(y))
        else:
            range = None
        return range

    #### Computation of elements to represent, figure limits and ticks.

    def compute_xy(self, agg='undefined'):
        if self.plotmode == 'bars':
            agg = max
        elif agg == 'undefined':
            agg = None
        return self.mode.compute_xy(agg=agg)

    def compute_bottom(self):
        return self.mode.compute_xy(agg=min)

    def compute_ylims(self):
        if self.plotmode == 'bars':
            arr = [ ymin for ymin in self.bottom]
            if not arr:
                arr = [51]
            minimum = (min(arr)-2)*0.9
            arr = [ ymax for ymax in self.y_orig]
            if not arr:
                arr = [59]
            maximum = (max(arr)+2)*1.1
            return [floor(minimum), ceil(maximum)]
        elif self.plotmode == 'plot':
            if self.y:
                minimum = (min(self.y)-2)*0.9
                maximum = (max(self.y)+2)*1.1
            else:
                minimum = (51-2)*0.9
                maximum = (59+2)*1.1
            return [floor(minimum), ceil(maximum)]

class SleepView(FigureView):
    """
    This is a figureview object that will display the user's sleep times.
    """

    #### Properties ##############

    viewmodes = (WeeklyViewMode(mode='bars'), MonthlyViewMode(mode='bars'), YearlyViewMode(mode='bars'))
    editable = True
    datatype = Activity
    activityproperty = 'Duration.total_seconds()/3600'

    @property
    def allowedtypes(self):
        return ['Sleep']

    @property
    def newitem(self):
        now = dt.datetime.now()
        time = dt.datetime(self.date.year, self.date.month, self.date.day, now.hour, now.minute, now.second, now.microsecond)
        time = attachlocaltimezone(time)
        item = Activity('NewActivity_' + time.isoformat(), \
                            Type='Sleep', \
                            StartTime=time, \
                            EndTime=time + dt.timedelta(hours=1), DeviceID='PyFit')
        return item

    # Computation of elements to represent, figure limits and ticks.

    def compute_xy(self):
        return self.mode.compute_xy(agg=mean)

    def compute_ylims(self):
        if not self.y:
            y = [0]
        else:
            y = self.y
        factor = max([ceil(max(y)/3), 3])
        return [0, factor*3]

    def compute_yticks(self):
        if not self.y:
            y = [0]
        else:
            y = self.y
        factor = max([ceil(max(y)/3), 3])
        locs = [ factor*i for i in range(0,4) ]
        labels = [ str(int(loc)).rjust(2) + ' ' + _g('h') for loc in locs ]
        return (locs, labels)

    #### Event handlers ##########

    def update_data(self):
        ''' In the sleep view one wants to consider only daily sums of sleep
        data, therefore there must be at most one entry for each day. In the
        following piece of code, whenever there are different sleep sessions
        for the same day, they are aggregated by summing them into a single one.
        '''
        super().update_data()
        dates = sorted(list(set([ EndTime.date() for EndTime in self.dates ])))
        data = [ sum([ self.data[i] for i in range(0, len(self.data)) if self.dates[i].date() == date ]) for date in dates ]
        self.dates = dates
        self.data = data

    def update_infolabel(self, aggmode='mean'):
        aggregated = super().update_infolabel(aggmode=aggmode)
        if aggregated:
            hours = floor(aggregated)
            minutes = (aggregated % 1) * 60
            self.infolabel.set_label(_g('{} {} {} {} on average').format(str(hours), _g('h'), str(round(minutes)), _g('m')))

class WeightView(FigureView):
    """
    This is a figureview object that will display the user's weight.
    """

    #### Properties ##############

    viewmodes = (WeeklyViewMode(mode='plot'), MonthlyViewMode(mode='plot'), YearlyViewMode(mode='plot'))
    datatype = Data
    editable = True
    units = _g('kg')

    @property
    def allowedtypes(self):
        return ['Weight']

    @property
    def newitem(self):
        now = dt.datetime.now()
        time = dt.datetime(self.date.year, self.date.month, self.date.day, now.hour, now.minute, now.second, now.microsecond)
        time = attachlocaltimezone(time)
        item = Data(Type='Weight', Time=time, Value=None, DeviceID='PyFit')
        return item

    #### Computation of elements to represent, figure limits and ticks.

    def get_datapoints(self, start, end, conn, type=None):
        datapoints = super().get_datapoints(start, end, conn, type=type)
        datapoints.insert(0, self.get_datapoint_prevnext(start, conn, sense='prev'))
        datapoints.append(self.get_datapoint_prevnext(end, conn, sense='next'))
        datapoints = [dp for dp in datapoints if dp]
        return datapoints

    @staticmethod # Exclusive for the WeightView, only applies to plot view.
    def get_datapoint_prevnext(date, conn, sense=None):
        cur = conn.cursor()
        if sense == 'prev':
            cur.execute("SELECT type, time FROM datapoints WHERE time = ( SELECT MAX(time) FROM datapoints WHERE type LIKE 'Weight' AND time < ? ) AND type LIKE 'Weight'", ( dt.datetime(date.year, date.month, date.day, 0), ))
        elif sense == 'next':
            cur.execute("SELECT type, time FROM datapoints WHERE time = ( SELECT MAX(time) FROM datapoints WHERE type LIKE 'Weight' AND time >= ? ) AND type LIKE 'Weight'", ( dt.datetime(date.year, date.month, date.day, 0) + dt.timedelta(days=1), ))
        else:
            raise Exception(_g('No direction specified.'))
        try:
            id = cur.fetchall()
        except:
            id = None
        if id:
            id = id[0]
            datapoint = Data.deserialize(id, conn=conn)
            return datapoint
        else:
            return None

    def compute_ylims(self):
        if self.y:
            return [min(self.y)*0.93, max(self.y)*1.07]
        else:
            return [50, 60]

    def compute_yticks(self):
        if self.y:
            locs = [ floor(min(self.y)*0.95), round(mean(self.compute_ylims())) , ceil(max(self.y)*1.05) ]
        else:
            locs = [51, 59]
        labels = [ str(int(loc)).rjust(2) + ' ' + self.units for loc in locs ]
        return (locs, labels)

    #### Event handlers ##########

    def update_infolabel(self, aggmode='range'):
            xlims = self.compute_xlims()
            if type(xlims[0]) == dt.date:
                lim0 = attachlocaltimezone(dt.datetime(xlims[0].year, xlims[0].month, xlims[0].day, 0, 0))
                lim1 = attachlocaltimezone(dt.datetime(xlims[1].year, xlims[1].month, xlims[1].day, 23, 59, 59))
            else:
                lim0 = xlims[0]
                lim1 = xlims[1]
            y = [ dp.Value for dp in self.datapoints if dp.Time >= lim0 and dp.Time <= lim1]
            if y:
                    aggregated = (min(y), max(y))
            else:
                aggregated = None
            if aggregated:
                self.infolabel.set_label(str(round(aggregated[0], 1)) + '-' + str(round(aggregated[1], 1)) + ' ' + self.units)
            else:
                self.infolabel.set_label(_g('No information'))
            return aggregated

class GoalsView(View):
    def __init__(self, date=dt.date.today()):
        self.initialized = False
        super().__init__(date=date)

        # Remove all the buttons and leave only the calendar button.
        for button in ['FastBack', 'FastForward', 'Back', 'Forward']:
            self.buttons[button].destroy()
            del self.buttons[button]
        self.buttons['Date'].set_relief(2)

        # Create the widget for each goal.
        self.goals = {}
        names = {'distancegoal': _g('Distance'),
                 'stepsgoal': _g('Steps'),
                 'minutesgoal': _g('Minutes')}
        for goal in ['distancegoal','stepsgoal', 'minutesgoal']:
            box = Gtk.VBox()
            databox = Gtk.Box()
            label = Gtk.Label()
            label.set_markup('<b>' + names[goal] + '</b>')
            label.set_xalign(0)
            amount = Gtk.Label()
            amount.set_xalign(2)
            imagebox = Gtk.Box()
            self.goals[goal] = {}
            self.goals[goal]['box'] = box
            self.goals[goal]['label'] = label
            self.goals[goal]['amount'] = amount
            self.goals[goal]['imagebox'] = imagebox
            box.pack_start(label, expand=True, fill=True, padding=0)
            databox.pack_start(imagebox, expand=False, fill=False, padding=0)
            databox.pack_start(amount, expand=True, fill=True, padding=0)
            box.pack_start(databox, expand=True, fill=True, padding=0)
            self.infobox.pack_start(box, expand=True, fill=True, padding=0)

        # Remove upper and lower box.
        self.remove(self.upperbox)
        self.remove(self.lowerbox)

        self.initialized = True
        self.infobox.show()
        self.update_gui()

    def update_data(self):
        # Compute the distance for the day.
        attrs = {'Distance': 'Distance',
                 'Steps': 'Steps',
                 'Duration': 'Minutes'}
        self.Distance = 0
        self.Steps = 0
        self.Minutes = 0
        conn = db.create_connection(platform.databasepath)
        activities = []
        for type in Activitytypes:
            activities.extend(Activity.fetch_by_date(self.date, conn=conn, type=type))
        for activity in activities:
            for attr in ['Distance', 'Steps']:
                if getattr(activity, attr) != None:
                    setattr(self, attrs[attr], getattr(self, attrs[attr]) + getattr(activity, attr))
            if getattr(activity, 'Duration') != None:
                setattr(self, attrs['Duration'], getattr(self, attrs['Duration']) + getattr(activity, 'Duration').total_seconds()/60)


    def update_gui(self):
        iconsize = 1
        if not self.initialized:
            return
        amounts = {'distancegoal': (str(round(self.Distance/1000,1)), _g('km')),
                      'stepsgoal': (str(round(self.Steps)), _g('steps')),
                      'minutesgoal': (str(round(self.Minutes)), _g('min'))}
        goals = ['distancegoal', 'stepsgoal', 'minutesgoal']
        items = [goal for goal in goals] + [ goal+'enabled' for goal in goals ]
        settings = db.read_settings(*items)
        for goal in goals:
            if settings[goal+'enabled']:
                self.goals[goal]['amount'].set_label(' '.join(amounts[goal]))
                self.goals[goal]['box'].show_all()
                if float(amounts[goal][0]) >= settings[goal]:
                    image = Gtk.Image.new_from_icon_name('gtk-ok', iconsize)
                else:
                    image = Gtk.Image.new_from_icon_name('gtk-dialog-warning', iconsize)
                for child in self.goals[goal]['imagebox'].get_children():
                    self.goals[goal]['imagebox'].remove(child)
                self.goals[goal]['imagebox'].pack_start(image, expand=False, fill=False, padding=10)
                for child in self.infobox.get_children():
                    if child == self.goals[goal]['box']:
                        break
                else:
                    self.infobox.pack_start(self.goals[goal]['box'], expand=True, fill=True, padding=0)
            else:
                self.infobox.remove(self.goals[goal]['box'])
        self.infobox.show_all()

#####################
## Activity Dialog ##
#####################

class AddEditDialog(Gtk.Dialog):
    """
    This dialog lets the user edit or add an activity or datapoint to the
    database. It works by initializing it with an item of the adequate type. The
    type will be detected by the dialog and it will adapt accordingly.

    At any time the attribute of the dialog 'item' can be queried. The item
    returned will be the original item that was provided to the dialog with the
    pertinent modifications that the user has made so far. This means that, even
    if the source items have properties that the dialog cannot yet handle, the
    information will not be lost.
    """

    #### Properties ##############

    @property
    def item(self):
        '''This property returns the item that the dialog is representing when retrieved.'''
        item = deepcopy(self._item)
        # Get the type.
        if self.controls.get('Type'):
            if self.controls.get('Type').get_active_text():
                item.Type = self.controls.get('Type').get_active_text()
        # Get the date.
        if self.controls.get('Date'):
            year, month, day = self.controls.get('Date').get_date()
            month += 1
            if type(self._item) == Activity:
                dst = time.localtime(dt.datetime.now().timestamp()).tm_isdst
                tz = dt.timezone(dt.timedelta(seconds=-time.timezone) + dst * dt.timedelta(seconds=3600))
                if item.StartTime.tzinfo and item.EndTime.tzinfo:
                    tz1 = item.StartTime.tzinfo
                    tz2 = item.EndTime.tzinfo
                else:
                    tz1 = tz
                    tz2 = tz
                start = (   int(self.controls['Start'].HourControl.get_value()),
                            int(self.controls['Start'].MinuteControl.get_value()),
                        )
                end   = (   int(self.controls['End'].HourControl.get_value()),
                            int(self.controls['End'].MinuteControl.get_value()),
                        )
                item.StartTime = dt.datetime(year, month, day, start[0], start[1], tzinfo=tz1)
                item.EndTime = dt.datetime(year, month, day, end[0], end[1], tzinfo=tz2)
                if item.EndTime < item.StartTime:
                    item.EndTime += dt.timedelta(days=1)
            elif type(self._item) == Data:
                if item.Time.tzinfo:
                    tz = item.Time.tzinfo
                hhmm = (    int(self.controls['Time'].HourControl.get_value()),
                            int(self.controls['Time'].MinuteControl.get_value()),
                       )
                item.Time = dt.datetime(year, month, day, hhmm[0], hhmm[1], tzinfo=tz)
        # Get attributes such as distance, calories, or the value if it is a data point.
        if type(item) == Activity:
            attributes = ( ('Distance', 1000), ('Calories', 1) )
        elif type(item) == Data:
            attributes = ( ('Value', 1), )
        for attribute, factor in attributes:
            if self.controls.get(attribute):
                if self.controls.get(attribute).get_value() > 0:
                    setattr(item, attribute, self.controls.get(attribute).get_value()*factor)
                else:
                    setattr(item, attribute, None)
        # Correct the ID to avoid integrity errors when adding two activities after another.
        if type(item) == Activity:
            if hasattr(item, 'isnew'):
                if item.isnew:
                    item.ID = (item.StartTime + dt.timedelta(seconds=dt.datetime.now().second, microseconds=dt.datetime.now().microsecond) ).isoformat()
        return item

    @item.setter
    def item(self, value):
        self._item = value
        # TODO: Update dialog if item is changed after initialization.

    @property
    def types(self):
        types = []
        if hasattr(self, '_types'):
            types.extend(self._types)
        try:
            types.remove(self.item.Type)
        except ValueError:
            pass
        types.insert(0, self.item.Type)
        return types

    @types.setter
    def types(self, value):
        self._types = value
        if self.controls.get('Type'):
            self.controls.get('Type').remove_all()
            for item in self.types:
                self.controls['Type'].append_text(item)
            self.controls.get('Type').set_active(0)
            if len(self.types) <= 1:
                self.boxes.get('Type').hide()
            else:
                self.boxes.get('Type').show()

    #### Initialization ##########

    def __init__(self, item=None, view=None):
        super().__init__(_g('Edit/Add'), view.get_toplevel(), 0,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_modal(True)
        self.set_default_size(150, 100)
        self.set_resizable(False)
        dialogbox = self.get_content_area()
        dialogbox.set_orientation(Gtk.Orientation.VERTICAL)
        dialogbox.set_border_width(10)
        dialogbox.set_spacing(10)
        if not item:
            item = view.newitem
        #- GUI Structure.
        self.controls = {}
        self.boxes = {}
        #-- Type selector
        typebox = Gtk.VBox()
        typelabel = Gtk.Label(_g('Type'))
        typecombobox = Gtk.ComboBoxText()
        self.controls['Type'] = typecombobox
        self.boxes['Type'] = typebox
        typebox.pack_start(typelabel, expand=False, fill=False, padding=0)
        typebox.pack_start(typecombobox, expand=False, fill=False, padding=0)
        dialogbox.pack_start(typebox, expand=False, fill=False, padding=0)
        #-- Datetime selector.
        datetimebox = Gtk.VBox(spacing=10)
        #--- Date
        datebox = Gtk.VBox()
        datelabel = Gtk.Label(_g('Date'))
        calendar = Gtk.Calendar()
        if type(item) == Activity:
            date = (item.StartTime.year, item.StartTime.month, item.StartTime.day)
        elif type(item) == Data:
            date = (item.Time.year, item.Time.month, item.Time.day)
        calendar.select_month(date[1]-1, date[0])
        calendar.select_day(date[2])
        self.controls['Date'] = calendar
        datebox.pack_start(datelabel, expand=True, fill=True, padding=0)
        datebox.pack_start(calendar, expand=True, fill=True, padding=0)
        #--- Time
        timebox = Gtk.Box()
        timebox.set_halign(Gtk.Align.CENTER)
        if type(item) == Activity:
            controls = { 'Start': TimeSelector(Time=item.StartTime),
                         'End': TimeSelector(Time=item.EndTime),
                       }
        elif type(item) == Data:
            controls = { 'Time': TimeSelector(Time=item.Time),
                       }
        #---- Start or ocurrence time.
        for name, control in controls.items():
            box = Gtk.VBox()
            self.controls[name] = control
            control.Label.set_label(name)
            box.pack_start(control, expand=True, fill=True, padding=0)
            box.set_halign(Gtk.Align.CENTER)
            timebox.pack_start(box, expand=True, fill=True, padding=0)
        #--- Pack everything into the datetimebox.
        datetimebox.pack_start(datebox, expand=True, fill=True, padding=0)
        datetimebox.pack_start(timebox, expand=True, fill=True, padding=0)
        dialogbox.pack_start(datetimebox, expand=True, fill=True, padding=0)
        # TODO: The clarity of the following part can be improved.
        #-- Activity or datapoint configurators.
        if type(item) == Activity:
            if not item.Type == 'Sleep':
                controls = { 'Distance': (_g('{} ({})').format(_g('Distance'), _g('km')), 0, 999, 0.1, 3, False, 10**(-3), 0, 'Distance'),
                             'Calories': (_g('Calories'), 0, 99999, 1, 0, True, 1, 0, 'Calories'),
                             }
            else:
                controls = {}
        elif type(item) == Data:
            controls = { 'Value': [item.Type, 0, 999, 0.1, 3, False, 1, 0, 'Value'],
                       }
            if item.Type == 'Weight':
                controls['Value'][0] += ' ({})'.format(_g('kg'))
                controls['Value'][1] = 0.1
                controls['Value'][4] = 1
            if item.Type == 'HeartRate':
                controls['Value'][0] = _g('Heart rate')
                controls['Value'][0] += _g(' ({})').format(_g('bpm'))
                controls['Value'][3] = 1
                controls['Value'][4] = 0
        for name, preset in controls.items():
            box = Gtk.VBox()
            label = Gtk.Label(preset[0])
            control = Gtk.SpinButton().new_with_range(preset[1], preset[2], preset[3])
            control.set_digits(preset[4])
            control.set_snap_to_ticks(preset[5])
            value = preset[7]
            if hasattr(item, preset[8]):
                x = getattr(item, preset[8])
                if x:
                    value = x*preset[6]
            control.set_value(value)
            self.controls[name] = control
            box.pack_start(label, expand=False, fill=False, padding=0)
            box.pack_start(control, expand=False, fill=False, padding=0)
            dialogbox.pack_start(box, expand=False, fill=False, padding=0)
        # Store attributes.
        self._item = item
        self.show_all()
        # Fix spacing of the timebox. #TODO: This is just a workaround, find a better method.
        timebox.set_spacing(timebox.get_allocation().width/4.5)