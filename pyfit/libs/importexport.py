'''This file contains the necessary logic to import data from different sources, i.e. a GadgetBridge database.'''

##### PyFit modules. #####
import libs.data as database
from libs.data import Activity, Data, Activitytypes
from libs.platform import databasepath
from libs.utils import attachlocaltimezone
###### Time and date #####
import datetime as dt
import time
######### Maths ##########
from math import ceil
######### Others #########
import os, ntpath
import subprocess
import csv
import zipfile, io
import sqlite3
import re
from xml.etree import ElementTree as et
import gettext
_g = gettext.gettext

#####################
#### GadgetBridge ###
#####################

def gbdataupdate():
    settings = database.read_settings()
    if settings['gbmethod'] == 'runscript':
        exitcode, message = gbrunupdatescript(path=settings['scriptpath'])
    else:
        exitcode, message = (True, _g('User script not required to be run.'))
    if exitcode:
        exitcode, message = gbdatabasecheck(path=settings['gbpath'])
    if exitcode:
        # Do the actual import.
        for device, table in GadgetbridgeTables.items():
            GadgetBridge(settings['gbpath'], mode=settings['gbimportmode'], table=table)
        exitcode, message = (True, _g('Import successful.'))
        if settings['gbremovefile'] == True:
            os.remove(settings['gbpath'])
    return exitcode, message

def gbrunupdatescript(path=database.read_settings()['scriptpath']):
    # Run the user's script and continue only if everything went good.
    try:
        userscript = subprocess.Popen(path, stdout=subprocess.PIPE)
        userscript.wait()
        exitcode = not bool(userscript.returncode)
        if not exitcode:
            message = _g('Error while executing the database update script: \n') + str(userscript.stdout.read())
        else:
            message = _g('Database update script successfully executed.')
    except (FileNotFoundError, PermissionError, OSError) as e:
        exitcode = False
        message = _g('The following error occurred while executing the database fetching program: ') + str(e)
    return exitcode, message

def gbdatabasecheck(path=database.read_settings()['gbpath']):
    if os.path.isfile(path):
        # Check if it really is a GadgetBridge database.
        conn = database.create_connection(path)
        cur = conn.cursor()
        try:
            cur.execute('SELECT name from sqlite_master where type= "table"')
            tables = cur.fetchall()
            conn.close()
            tablelist = ['ACTIVITY_DESCRIPTION', 'BASE_ACTIVITY_SUMMARY', 'USER', 'USER_ATTRIBUTES', 'DEVICE_ATTRIBUTES']
            if len([table[0] for table in tables if table[0] in tablelist]) != len(tablelist):
                exitcode, message = (False, _g('The selected file is not a GadgetBridge database.'))
            else:
                exitcode, message = (True, _g('The selected file is a GadgetBridge database.'))
        except sqlite3.DatabaseError:
            conn.close()
            exitcode, message = (False, _g('The selected file is not a GadgetBridge database.'))
    else:
        exitcode, message = (False, _g('The GadgetBridge database file does not exist.'))
    return exitcode, message

# TODO: Support other devices rather than only MI Bands.
GadgetbridgeTables = {'Huami': 'MI_BAND_ACTIVITY_SAMPLE'}

class GadgetBridge:
    '''This is the class that takes care of importing data from GadgetBridge. It should be
    deleted after being initialized, so that the connections to the database are closed.'''

    #TODO: Support for multiple devices.

    #### Properties ##############
    tolerances = {'Walking': (3, 3),
                  'Running': (3, 3),
                  'Sleep':   (10, 15),
                  }
    chunksize = 60 # Chunk size for heart rate readings.

    #### Initialization ##########
    def __init__(self, path, mode='Refresh', table='gbtable', conn=None, gbconn=None):
        self.gbtable = table
        # Connect both to PyFit database and GadgetBridge database.
        self.conn = conn
        if not self.conn:
            self.conn = database.create_connection(databasepath)
        self.gbconn = gbconn
        if not self.gbconn:
            self.gbconn = database.create_connection(path)
        time = dt.datetime.now()
        if mode == 'Overwrite':
            cur = self.conn.cursor()
            cur.execute('DELETE FROM activities WHERE NOT deviceid="PyFit" AND deviceid IS NOT NULL')
            self.conn.commit()
            cur.execute('DELETE FROM datapoints WHERE NOT deviceid="PyFit" AND deviceid IS NOT NULL')
            self.conn.commit()
        self.refresh_heartrate()
        self.refresh_activities()
        time = dt.datetime.now() - time
        print(_g('Database updated in {} {}.').format(str(time.total_seconds()), _g('seconds')))

    #### Destruction #############
    def __del__(self):
        '''It is essential to delete the importer, otherwise the connections to the database remain open.'''
        self.conn.commit()
        self.conn.close()
        self.gbconn.close()

    #### Heart rate ###########

    def refresh_heartrate(self):
            # Find maximum timestamp in the dataset.
            cur = self.conn.cursor()
            cur.execute('''SELECT time FROM datapoints WHERE time = ( SELECT MAX(time) FROM datapoints WHERE NOT 
                        deviceid="PyFit")''')
            try:
                start = cur.fetchall()[0][0]
            except:
                start = None
            if start:
                start = dt.datetime.fromisoformat(start)
                start = dt.datetime(start.year, start.month, start.day, start.hour, start.minute, tzinfo=start.tzinfo) + dt.timedelta(minutes=1)
                start = start.timestamp()
            # Import from computed start point.
            self.import_heartrate(start=start)

    def import_heartrate(self, start=None, end=None):
        gbcur = self.gbconn.cursor()
        if not start:
            # Find minimum in the dataset.
            gbcur.execute("SELECT timestamp FROM {} WHERE timestamp = ( SELECT MIN(timestamp) FROM {})".format(self.gbtable, self.gbtable))
            start = gbcur.fetchall()[0][0]
        if not end:
            # Find maximum timestamp in the dataset.
            gbcur.execute("SELECT timestamp FROM {} WHERE timestamp = ( SELECT MAX(timestamp) FROM {})".format(self.gbtable, self.gbtable))
            end = gbcur.fetchall()[0][0]
        # This splits the specified range in chunks and imports chunk by chunk.
        count = 0
        for i in range(0, int(ceil(end-start+1)/(self.chunksize*60))):
            datapointlist = []
            chunk = self.fetch_chunk(self.gbconn, start + i*self.chunksize*60, start + (i+1)*self.chunksize*60-1)
            # Filter out readings without heart rate.
            chunk = [ dp for dp in chunk if dp[-1] != 255 ]
            # Save all the readings in the data point list.
            for datapoint in chunk:
                data = Data(Type='HeartRate', Time=attachlocaltimezone(dt.datetime.fromtimestamp(datapoint[0])), Value=datapoint[-1],
                            DeviceID=self.get_deviceid(attachlocaltimezone(dt.datetime.fromtimestamp(datapoint[0]))))
                datapointlist.append(data)
            # Write all the data points to the database.
            for data in datapointlist:
                data.writetodatabase(self.conn)
                count += 1
        self.conn.commit()

    #### Activities ###########

    def refresh_activities(self):
        # Find maximum timestamp in the dataset.
        cur = self.conn.cursor()
        cur.execute(
            '''SELECT endtime FROM activities WHERE endtime = ( SELECT MAX(endtime) FROM activities WHERE NOT 
            deviceid="PyFit")''')
        try:
            start = cur.fetchall()[0][0]
        except:
            start = None
        if start:
            start = dt.datetime.fromisoformat(start)
            start = dt.datetime(start.year, start.month, start.day, start.hour, start.minute,
                                tzinfo=start.tzinfo) + dt.timedelta(minutes=1)
            start = start.timestamp()
        # Import from computed starting point.
        self.import_activities(start=start)

    def import_activities(self, start=None, end=None):
        activitylist = []
        for type in ['Walking','Running','Sleep']:
            activitylist.extend(self.read_activities(type=type, start=start, end=end))
        # Filter the activities.
        for type in ['Walking', 'Running']:
            activitylist = self.filter_by_type(activitylist, type=type, filterfunction=self.filter_duration)
            activitylist = self.filter_by_type(activitylist, type=type, filterfunction=self.filter_distance)
        activitylist = self.filter_by_type(activitylist, type='Sleep', filterfunction=lambda x: self.filter_duration(x, minimum=21))
        activitylist = self.filter_by_type(activitylist, type='Walking', filterfunction=lambda x: self.filter_speed(x, limits=(0.40, 2)))
        activitylist = self.filter_by_type(activitylist, type='Running', filterfunction=lambda x: self.filter_speed(x, limits=(1.2, 11)))
        activitylist = self.filter_overlap(activitylist)
        activitylist = self.filter_overlap_user(activitylist)
        print(activitylist)
        for activity in activitylist:
            try:
                activity.writetodatabase(self.conn)
            except:
                #TODO: Decide what to do in this case.
                #print('WARNING: repeated activity.')
                pass

    def read_activities(self, start=None, end=None, type=None):
        gbcur = self.gbconn.cursor()
        '''Set the starting time and ending times as the minimum and maximum times
        of any entry in the database whenever they are not given.'''
        query = "SELECT timestamp FROM {} WHERE timestamp = ( SELECT {}(timestamp) FROM {})"
        if not start:
            gbcur.execute(query.format(self.gbtable, 'MIN', self.gbtable))
            start = gbcur.fetchall()[0][0]
        if not end:
            gbcur.execute(query.format(self.gbtable, 'MAX', self.gbtable))
            end = gbcur.fetchall()[0][0]
        if not type:
            raise Exception(_g('Importing activities from GadgetBridge: no type specified.'))
        tolerance = self.tolerances[type][0]
        extendedtolerance = self.tolerances[type][1]
        # Execute an algorithm that scans the dataset for the requested activity
        # type.
        activitylist = []
        cursor = start // 60 * 60 # The cursor is in seconds, this rounds it down to the closest minute.
        while cursor < end: # In each iteration the algorithm tries to find a single activity.
            activitysublist, cursor = self.streamscan(type=type, start=cursor, datasetend=end, tolerance=tolerance, extendedtolerance=extendedtolerance)
            if activitysublist:
                activitylist.extend(activitysublist)
        return activitylist

    def streamscan(self, type=None, start=None, datasetend=None, tolerance=3, extendedtolerance=9):
        chunk = []
        mask = []
        mask_extended = []
        limits = [None, None]
        cursor = start // 60 * 60
        firstfetch = False
        timemask = None
        prev = False
        prev_extended = False
        activitylist = []
        while cursor < datasetend and (not limits[0] or not limits[1]):
            useful = False
            buffer = self.fetch_chunk(self.gbconn, cursor, cursor + extendedtolerance * 60 + 2*tolerance * 60 )
            cursor += extendedtolerance * 60 + 2 * tolerance * 60 + 60
            if not buffer or not buffer and not firstfetch:
                continue
            if buffer and chunk:
                if buffer[0][0] != chunk[-1][0] + 60:
                    # Missing data, discard chunk.
                    limits[0] = None
                    chunk = []
                    mask = []
                    mask_extended = []
            firstfetch = True
            buffer_mask, buffer_mask_extended, timemask = self.identify_activity(buffer, type=type, prev=prev, prev_extended=prev_extended, timemask=timemask, tolerance=tolerance, extendedtolerance=extendedtolerance)
            prev = buffer_mask[-1]
            prev_extended = buffer_mask_extended[-1]
            if not limits[0]:
                distance = self.mask_first_true(buffer_mask, 0, 1)
                if distance != None:
                    limits[0] = distance
                    useful = True
            elif not limits[1]:
                useful = True
                distance = self.mask_first_true([ buffer_mask[i] or buffer_mask_extended[i] for i in range(0,len(buffer_mask)) ], len(buffer)-1, -1)
                if distance != None:
                    if distance > tolerance:
                        firsttrue = self.mask_first_true(buffer_mask, len(buffer)-1, -1)
                        if firsttrue:
                            limits[1] = len(chunk) + len(buffer) - firsttrue
                        else:
                            limits[1] = len(chunk) - self.mask_first_true(mask, len(buffer)-1, -1) - 1
                else:
                    limits[1] = len(chunk) + - 1
            if useful:
                chunk.extend(buffer)
                mask.extend(buffer_mask)
                mask_extended.extend(buffer_mask_extended)
        else:
            if limits[0] and limits[1]:
                # Activity found, return it
                mask = [ mask[i] for i in range(0, len(chunk)) if i >= limits[0] and i <= limits[1]]
                mask_extended = [ mask_extended[i] for i in range(0, len(chunk)) if i >= limits[0] and i <= limits[1]]
                chunk = [ chunk[i] for i in range(0,len(chunk)) if i >= limits[0] and i <= limits[1] ]
                mask = self.upgrade_mask(mask, tolerance=tolerance, auxiliarymask=mask_extended)
                blocks = self.mask_to_blocks(mask)
                for block in blocks:
                    achunk = [chunk[i] for i in range(block[0], block[1]+1) if mask[i] ]
                    activity = self.chunk_to_activity(achunk, type=type)
                    activitylist.append(activity)
            return (activitylist, cursor)

    def identify_activity(self, chunk, type=None, tolerance=3, extendedtolerance=9, prev=False, prev_extended=False, timemask=None):
        tolerance = tolerance * 60
        extendedtolerance = extendedtolerance * 60
        mask = [ False for item in chunk ]
        mask_extended = [ False for item in chunk ]
        if type == 'Sleep':
            for i in range(0, len(mask)):
                if i == 0 and prev:
                    timemask = chunk[i][0] - 60 + extendedtolerance
                if int(format(chunk[i][3],'b')[-4:], 2) in [9, 11]: # Light or deep sleep.
                    mask[i] = True
                    timemask = chunk[i][0] + extendedtolerance
                elif (prev or prev_extended) and chunk[i][3] in [112, 122]:
                    mask[i] = True
                    timemask = chunk[i][0] + extendedtolerance
                elif int(format(chunk[i][3],'b')[-4:], 2) in [12]:
                    if i != len(mask)-1:
                        if int(format(chunk[i-1][3],'b')[-4:], 2) in [3] and int(format(chunk[i+1][3],'b')[-4:], 2) in [3] and prev:
                            mask_extended[i] = True
                        else:
                            timemask = None
                elif int(format(chunk[i][3],'b')[-4:], 2) not in [0, 3, 10]:
                    timemask = None
                elif timemask:
                    if chunk[i][0] <= timemask and int(format(chunk[i][3],'b')[-4:], 2) in [0, 10]:
                        mask_extended[i] = True
                prev = mask[i]
                prev_extended = mask_extended[i]
        elif type == 'Walking':
            for i in range(0,len(mask)):
                if int(format(chunk[i][3],'b')[-4:], 2) in [1]:
                    mask[i] = True
                else:
                    mask[i] = False
        elif type == 'Running':
            for i in range(0, len(mask)):
                if int(format(chunk[i][3],'b')[-4:], 2) in [2]:
                    mask[i] = True
                elif int(format(chunk[i][3],'b')[-4:], 2) in [1]:
                    mask_extended[i] = True
        return (mask, mask_extended, timemask)

    def upgrade_mask(self, mask, tolerance=3, auxiliarymask=None):
        # Create number mask.
        blocks = self.mask_to_blocks(mask)
        nmask = [ 0 for item in mask ]
        maxsize = 0
        for block in blocks:
            size = block[1] - block[0] + 1
            units = min(tolerance, size)
            maxsize = max(size, maxsize)
            nmask[block[0]:block[1] + 1] = [units for i in range(0, size)]
        # Create objects.
        objects = []
        blocks = self.mask_to_blocks(mask)
        for block in blocks:
            for i in [0,1]:
                dir = {0: -1, 1: 1}[i]
                object = [nmask[block[i]], dir, block[i], block[i]]
                objects.append(object)
        # Compute intervals.
        for i in range(0,len(objects)):
            while objects[i][0] > 0:
                initial = objects[i][3]
                objects[i][3] = objects[i][3] + objects[i][1]*objects[i][0]
                objects[i][0] = 0
                start, end = (min(objects[i][2], objects[i][3]), max(objects[i][2], objects[i][3]))
                start = max(0, start); end = min(len(mask)-1, end)
                if auxiliarymask and not (start == 0 and objects[i][1] == -1) and not (end == len(mask)-1 and objects[i][1] == 1):
                    objects[i][0] += sum([int(auxiliarymask[x]) for x in range(initial+1, end+1)])
            objects[i][2:] = [start, end]
        # Compute collisions.
        collisions = []
        for j in range(1,len(objects)):
            for i in range(0, j):
                if objects[i][1] != objects[j][1]:
                    if objects[i][1] == 1 and objects[i][2] <= objects[j][3] and objects[i][3] >= objects[j][2]:
                        collisions.append((objects[i][2], objects[j][3]))
                    elif objects[i][1] == -1 and objects[j][2] <= objects[i][3] and objects[j][3] >= objects[i][2]:
                        collisions.append((objects[j][2], objects[i][3]))
        for start, end in collisions:
            mask[start:end+1] = [True for x in range(start, end+1)]
        return mask

    def chunk_to_activity(self, chunk, type=None):
        # Compute starting and ending times.
        if not chunk:
            return None
        starttime = chunk[0][0] - 60
        endtime = chunk[-1][0]
        # Do stuff if the timestamps are in utc.
        if not self.isgbtime(dt.datetime.fromtimestamp(starttime)):
            starttime += -time.timezone
            endtime += -time.timezone
        dst = time.localtime(starttime).tm_isdst
        tz = dt.timezone(dt.timedelta(seconds=-time.timezone) + dst * dt.timedelta(seconds=3600))
        if not self.isgbtime(dt.datetime.fromtimestamp(starttime)) and dst:
            starttime += 3600
            endtime += 3600
        StartTime = dt.datetime.fromtimestamp(starttime, tz=tz)
        EndTime = dt.datetime.fromtimestamp(endtime, tz=tz)
        kwargs = {'Type': type, 'StartTime': StartTime, 'EndTime': EndTime, 'DeviceID': self.get_deviceid(EndTime)}
        if type == 'Walking' or type == 'Running':
            height = self.get_userdata_at_time(StartTime)[2]
            steps = 0
            for dp in chunk:
                steps += dp[2]
            # Source of distance calculation: https://livehealthy.chron.com/determine-stride-pedometer-height-weight-4518.html
            Distance = steps * height * 0.414 / 100  # In cm that's why / 100
            kwargs['Distance'] = Distance
            kwargs['Steps'] = steps
        activity = Activity(StartTime.isoformat(), **kwargs)
        return activity

    #### Activity filters ########

    '''When a filter is executed, it returns True if the activity is kept and False if it is removed.'''

    #--- Individual filters.

    def filter_distance(self, activity, minimum=200):
        result = True
        if activity.Distance < minimum:
            result = False
        return result

    def filter_duration(self, activity, minimum=4):
        result = True
        if activity.EndTime - activity.StartTime < dt.timedelta(minutes=minimum):
            result = False
        return result

    def filter_speed(self, activity, limits=(0, None)):
        speed = activity.Distance / (activity.EndTime - activity.StartTime).total_seconds()
        result = True
        # Source for speed: Unknown.
        if limits[0] != None:
            if speed <= limits[0]:
                result = False
        if limits[1] != None:
            if speed >= limits[1]:
                result = False
        return result

    #--- Collective filters.

    def filter_by_type(self, activitylist, type=None, filterfunction=None):
        for i in range(0, len(activitylist)):
            if activitylist[i].Type == type:
                if not filterfunction(activitylist[i]):
                    activitylist[i] = None
        return [ act for act in activitylist if act != None ]

    def filter_overlap(self, activitylist, hierarchy=('Sleeping', 'Running', 'Walking')):
        for i in range(1, len(hierarchy)):
            sublevelactivities = [ activity for activity in activitylist if activity.Type in hierarchy[i:] ]
            indexestoremove = set()
            for activity in [ activity for activity in activitylist if activity.Type == hierarchy[i-1]]:
                for j in range(0,len(sublevelactivities)):
                    # We check now if the activity overlaps with three checks. If all of them fail we skip to the next
                    # activity, otherwise we add the activity to the set of indexes to remove.
                    if sublevelactivities[j].StartTime >= activity.StartTime and sublevelactivities[j].StartTime < activity.EndTime:
                        pass # Activity starts inside and ends outside.
                    elif sublevelactivities[j].EndTime > activity.StartTime and sublevelactivities[j].EndTime <= activity.EndTime:
                        pass # Activity starts outside and ends outside.
                    elif sublevelactivities[j].StartTime >= activity.StartTime and sublevelactivities[j].EndTime <= activity.EndTime:
                        pass #Activity is inside.
                    else:
                        continue # No conflict.
                    indexestoremove.add(j)
            for j in indexestoremove:
                activitylist[j] = None
                activitylist = [ act for act in activitylist if act != None ]
        return activitylist

    def filter_overlap_user(self, activitylist):
        times = [ act.StartTime for act in activitylist ]
        if times:
            start = min(times) - dt.timedelta(days=1)
            end = max(times) + dt.timedelta(days=1)
        else:
            return []
        activities = []
        for t in Activitytypes:
            fetch = Activity.fetch_by_date(start.date(), end=end.date(), conn=self.conn, type=t)
            activities += fetch
        for i in range(0, len(activities)):
            for j in range(0, len(activitylist)):
                if activitylist[j]:
                    if (activities[i].StartTime >= activitylist[j].StartTime and activities[i].StartTime <= activitylist[j].EndTime) \
                        or (activities[i].EndTime >= activitylist[j].StartTime and activities[i].EndTime <= activitylist[j].EndTime) \
                        or (activities[i].StartTime <= activitylist[j].StartTime and activities[i].EndTime >= activitylist[j].EndTime):
                        # There is overlap.
                        activitylist[j] = None
        activitylist = [ act for act in activitylist if act != None]
        return activitylist

    #### Auxiliary functions #####

    def get_deviceid(self, time):
        query = '''SELECT DEVICE.IDENTIFIER FROM 
                   DEVICE_ATTRIBUTES INNER JOIN DEVICE ON DEVICE_ATTRIBUTES.DEVICE_id=DEVICE._id 
                   WHERE DEVICE_ATTRIBUTES.VALID_FROM_UTC <= ? AND 
                   CASE 
                    WHEN DEVICE_ATTRIBUTES.VALID_TO_UTC IS NOT NULL THEN DEVICE_ATTRIBUTES.VALID_FROM_UTC >= ?
                    ELSE TRUE 
                   END'''
        time = round(time.timestamp()*1000)
        gbcur = self.gbconn.cursor()
        gbcur.execute(query, (time, time))
        try:
            deviceid = gbcur.fetchone()[0]
        except TypeError as e:
            deviceid = 'Unknown GadgetBridge device'
        return deviceid

    def get_userdata_at_time(self, time):
        # For now assume that time is a time aware datetime.
        # Also assume there is only one user with id 1.
        time = time.timestamp()
        now = dt.datetime.now().timestamp() + 1
        gbcur = self.gbconn.cursor()
        gbcur.execute("SELECT NAME, BIRTHDAY, GENDER FROM USER where _id=1")
        user = gbcur.fetchall()[0]
        gbcur.execute("SELECT VALID_FROM_UTC, VALID_TO_UTC, HEIGHT_CM, WEIGHT_KG FROM USER_ATTRIBUTES WHERE USER_ID=1")
        userdata = gbcur.fetchall()
        for i in range(0,len(userdata)):
            userdata[i] = list(userdata[i])
            userdata[i][0] = userdata[i][0]/1000
            if userdata[i][1] == None:
                userdata[i][1] = now
            else:
                userdata[i][1] = userdata[i][1]/1000
        datatouse = None
        for data in userdata:
            if time >= data[0] and time < data[1]:
                datatouse = data
        if datatouse == None:
            datatouse = min(userdata, key=lambda x: x[0])
        return (user[2], dt.datetime.fromtimestamp(now) -
                dt.datetime.fromtimestamp(user[1]/1000), datatouse[2],
                datatouse[3])

    def isgbtime(self, time):
        time = time.timestamp()
        gbcur = self.gbconn.cursor()
        gbcur.execute("SELECT VALID_FROM_UTC FROM USER_ATTRIBUTES WHERE USER_ID=1")
        userdata = gbcur.fetchall()
        userdata = [ data[0]/1000 for data in userdata ]
        userdata = min(userdata)
        return time >= userdata

    def fetch_chunk(self, gbconn, start, end):
        # Do not fill empty spaces.
        cur = gbconn.cursor()
        query = "SELECT timestamp, raw_intensity, steps, raw_kind, heart_rate FROM {} WHERE timestamp >= ? and timestamp <= ?".format(self.gbtable)
        cur.execute(query, (start, end))
        chunk = cur.fetchall()
        # Filter out invalid readings.
        chunk = [ dp for dp in chunk if not (dp[1] == -1 and dp[2] == 0
                    and (dp[-1] == -1 or dp[-1] == 0)) ]
        return sorted(chunk, key= lambda dp: dp[0])

    def mask_first_true(self, mask, start, factor):
        distance = None
        j = start
        while j >= 0 and j < len(mask):
            if mask[j]:
                distance = abs(j-start)
                break
            j = j + factor * 1
        return distance

    def mask_to_blocks(self, mask):
        blocks = []
        if mask[0]:
            block = [0]
        for i in range(1,len(mask)-1):
            if mask[i] and not mask[i-1]:
                block = [i]
            if not mask[i] and mask[i-1]:
                block.append(i-1)
                blocks.append(tuple(block))
        if len(mask) > 1:
            if mask[-1] and not mask[-2]:
                blocks.append((len(mask)-1, len(mask)-1))
            elif mask[-1] and mask[-2]:
                block.append(len(mask)-1)
                blocks.append(tuple(block))
            elif not mask[-1] and mask[-2]:
                block.append(len(mask)-2)
                blocks.append(tuple(block))
        return blocks

#####################
#### Google Fit #####
#####################
'''This code is here for possible future use, but it is not production code.
importing from Google Fit is not supported for now.'''

class GoogleFitImporter:
    locales = {  'es-ES': ('Actividades', 'Agregaciones diarias'),
              }
    importfunctions =   {   '.zip': 'import_zip',
                        }
    maxfilesinmemory = 50

    def __init__(self, path):
        # Check validity and locale of input file.
        self.type = os.path.splitext(path)[-1]
        self.locale = None
        if self.type == '.zip': # Zip file
            archive = zipfile.ZipFile(path, 'r')
            for locale, names in self.locales.items():
                curindex = 0
                for item in archive.namelist():
                    if item.find('Takeout/Fit/' + names[curindex]) != -1:
                        curindex += 1
                        if curindex == 2:
                            break
                if curindex == 2:
                    self.locale = locale
                    self.path = path
                    print('Valid file')
                    break
            else:
                print('Invalid file.')
                raise Exception('Invalid file')
        else:
            print('Invalid file.')
            raise Exception('Invalid file')

    def performimport(self):
        importfunction = self.importfunctions.get(self.type)
        if importfunction:
            eval('self.' + importfunction + '()' )

    def import_zip(self):
        database.init_database()
        activityfolder = self.locales[self.locale][0]
        dailyaggfolder = self.locales[self.locale][1]
        archive = zipfile.ZipFile(self.path, 'r')
        self.archive = archive
        conn = database.create_connection(databasepath)

        # Import activities.
        #- Determine which are the files to import
        files = archive.namelist()
        files = [ file for file in files if file.find(activityfolder) != -1 ]
        files = [ file for file in files if file[-4:] == '.tcx' ]
        #- Start to import, it is node in blocks.
        readfiles = 0
        lenfiles = len(files)
        while readfiles < lenfiles: # Main loop for importing.
            print('Importing activity files: ' + str(round(readfiles/lenfiles*100, 2)) + '%.')
            # Start block of files.
            filesinmemory = 0
            activities = []
            while filesinmemory < self.maxfilesinmemory and readfiles < lenfiles:
                    file = files.pop()
                    # Start fixing the timezone from the filename.
                    timezonestr = os.path.split(file)[-1]
                    span = re.search('\+.*_PT|-.{5}_PT', timezonestr).span()
                    timezonestr = timezonestr[span[0]:span[1]-3].replace('_',':')
                    if timezonestr[0] == '+':
                        factor = 1
                    elif timezonestr[1] == '-':
                        factor = -1
                    timedelta = factor*dt.timedelta(hours=int(timezonestr[1:3]), minutes=int(timezonestr[4:]))
                    archivetext = archive.read(file).decode('utf-8')
                    archivetext = archivetext.replace('Z">', timezonestr + '">')
                    archivetext = archivetext.replace('Z</Time>', timezonestr + '</Time>')
                    matches = [m.start() for m in re.finditer( timezonestr[1:] + '">', archivetext)] + [m.start() for m in re.finditer(timezonestr[1:] + '</Time>', archivetext)]
                    stringpairs = []
                    for match in matches:
                        string = archivetext[match-24:match+5]
                        datet = dt.datetime.fromisoformat(string)
                        datet += timedelta
                        newstring = datet.isoformat()
                        stringpairs.append((string, newstring))
                    for old, new in stringpairs:
                        archivetext = archivetext.replace(old, new)
                    # Timezone fixed, now the file can be read.
                    bytes_io = io.BytesIO(archivetext.encode())
                    fileactivities = readtcx(bytes_io)
                    for activity in fileactivities:
                        activity.Track = self.cleantrack(activity.Track)
                        for item in activity.Track:
                            item['Time'] += timedelta
                    activities.extend(fileactivities)
                    readfiles += 1
                    filesinmemory += 1
            # Finished block read, write activities to database and commit.
            #- Filter the activities
            activities = [ self.filteractivity(activity) for activity in activities ]
            activities = [ activity for activity in activities if activity ]
            if activities:
                for activity in activities:
                    try:
                        #print(activity.Id, activity.Track, '\n')
                        activity.writetodatabase(conn)
                    except sqlite3.IntegrityError as e:
                        # But it is possible that an activity with the same id already exists.
                        # In that case if the new activity is longer, it overwrites the old activity
                        if str(e) == 'UNIQUE constraint failed: activities.id':
                            existingactivity = Activity.deserialize(activity.ID, conn=conn)
                            if activity.Distance > existingactivity.Distance:
                                # Old activity overwritten.
                                existingactivity.deletefromdatabase(conn)
                                activity.writetodatabase(conn)
                            else:
                                pass # Old activity not overwritten.
                        else:
                            # If it is anoter kind of error then the exception shall be thrown again.
                            raise sqlite3.IntegrityError(str(e))
            del activities
            conn.commit()

        # Import sleep data.
        '''This time one has to read all files at once since,
        otherwise the sleep blocks could be splitted.'''
        files = archive.namelist()
        files = [ file for file in files if file.find(dailyaggfolder) != -1 ]
        files = [ file for file in files if file[-4:] == '.csv' ]
        files = [ file for file in files if len(file.split('/')[-1]) == 14 ]
        sleeplist = []
        for file in files:
            #bytes_io = io.BytesIO(archive.read(file))
            sleeplist.extend(self.readsleepday(file))
        sleeplist = self.readsleepchunks(sleeplist)
        count = 0
        total = len(sleeplist)
        if sleeplist:
            for activity in sleeplist:
                count += 1
                #print('Importing sleep data: ' + str(count/total*100) + '%.')
                activity.writetodatabase(conn)
        del sleeplist
        conn.commit()

        # Import weight data.
        #- Reuse files from sleep data.
        weightlist = []
        for file in files:
            weightlist.extend(self.readweightday(file))
        count = 0
        total = len(weightlist)
        if weightlist:
            for datapoint in weightlist:
                count += 1
                #print('Importing weight data: ' + str(count/total*100) + '%.')
                datapoint.writetodatabase(conn)
        del weightlist
        conn.commit()

        # Import heart rate data.
        #- Reuse files from sleep data.
        heartlist = []
        for file in files:
            heartlist.extend(self.readheartday(file))
        count = 0
        total = len(heartlist)
        if heartlist:
            for datapoint in heartlist:
                count += 1
                #print('Importing heart rate data: ' + str(count/total*100) + '%.')
                datapoint.writetodatabase(conn)
        del heartlist
        conn.commit()
        conn.close()

    def readsleepday(self, filename):
        '''Returns sleep activities for each 15min of the day (if they existed).'''
        sleeplist = []
        if not self.type == '.zip':
            csvfile = open(filename, 'r')
        else:
            csvfile = io.StringIO(io.BytesIO(self.archive.read(filename)).read().decode('UTF-8'))
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        header = next(reader)
        for row in reader:
            rowdict = {header[i]: row[i] for i in range(0,len(header))}
            sleepdict = {}
            sleepdict['StartTime'] = dt.datetime.fromisoformat(ntpath.basename(filename)[0:-4] + 'T' + row[0])
            sleepdict['EndTime'] = dt.datetime.fromisoformat(ntpath.basename(filename)[0:-4] + 'T' + row[1])
            for key in ['Dormir duración (ms)', 'Sueño ligero duración (ms)', 'Sueño profundo duración (ms)', 'Desvelo duración (ms)']:
                if rowdict.get(key) in ['', None]:
                    rowdict[key] = 0
                else:
                    rowdict[key] = int(rowdict[key])
            sleepdict['Duration'] = sum( [ rowdict[key] for key in ['Dormir duración (ms)', 'Sueño ligero duración (ms)', 'Sueño profundo duración (ms)', 'Desvelo duración (ms)'] ] ) # In miliseconds
            if sleepdict['Duration'] > 0:
                sleeplist.append(sleepdict)
        csvfile.close()
        return sleeplist

    def readsleepchunks(self, sleeplist):
        """Takes a list with sleeping chunks with a maximum
        size of 15 minutes and joins adjacent chunks to
        create a sleep activity."""
        sleeplist = sorted(sleeplist, key = lambda i: i['StartTime'])
        chunklist = []
        chunkongoing = False
        for period in sleeplist:
            if chunkongoing == False and period['Duration'] > 0:
                chunkongoing = True
                chunkdict = {}
                chunkdict['StartTime'] = period['EndTime'] - dt.timedelta(milliseconds=period['Duration'])
                chunkdict['EndTime'] = period['EndTime']
            elif chunkongoing == True:
                chunkdict['EndTime'] = chunkdict['EndTime'] + dt.timedelta(milliseconds=period['Duration'])
                if period['Duration'] < (period['EndTime'] - period['StartTime']).seconds*1000 + (period['EndTime'] - period['StartTime']).microseconds/1000:
                    chunkongoing = False
                    chunklist.append(chunkdict)
        activitylist = []
        for chunk in chunklist:
            activity = Activity(chunk['StartTime'].isoformat() + '_sleep', Type='Sleep', StartTime=chunk['StartTime'], EndTime=chunk['EndTime'], Track=[])
            activitylist.append(activity)
        return activitylist

    def readweightday(self, filename):
        if not self.type == '.zip':
            csvfile = open(filename, 'r')
        else:
            csvfile = io.StringIO(io.BytesIO(self.archive.read(filename)).read().decode('UTF-8'))
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        header = next(reader)
        weightlist = []
        for row in reader:
            rowdict = {header[i]: row[i] for i in range(0,len(header))}
            weightdict = {}
            weightdict['Time'] = dt.datetime.fromisoformat(ntpath.basename(filename)[0:-4] + 'T' + row[0]) \
                + 1/2*(dt.datetime.fromisoformat(ntpath.basename(filename)[0:-4] + 'T' + row[1]) \
                - dt.datetime.fromisoformat(ntpath.basename(filename)[0:-4] + 'T' + row[0]))
            for key in ['Peso medio (kg)', 'Peso máximo (kg)', 'Peso mínimo (kg)']:
                if rowdict.get(key) in ['', None]:
                    rowdict[key] = None
                else:
                    rowdict[key] = float(rowdict[key])
            weightdict['Value'] = rowdict['Peso medio (kg)']
            weightdict['Min'] = rowdict['Peso mínimo (kg)']
            weightdict['Max'] = rowdict['Peso máximo (kg)']
            if weightdict['Value'] != None:
                weightitem = Data(Type='Weight', Time=weightdict['Time'], Value=weightdict['Value'])
                if weightdict['Min'] and weightdict['Max']:
                    weightitem.Min = weightdict['Min']
                    weightitem.Max = weightdict['Max']
                weightlist.append(weightitem)
        csvfile.close()
        return weightlist

    def readheartday(self, filename):
        if not self.type == '.zip':
            csvfile = open(filename, 'r')
        else:
            csvfile = io.StringIO(io.BytesIO(self.archive.read(filename)).read().decode('UTF-8'))
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        header = next(reader)
        heartlist = []
        for row in reader:
            rowdict = {header[i]: row[i] for i in range(0,len(header))}
            heartdict = {}
            heartdict['Time'] = dt.datetime.fromisoformat(ntpath.basename(filename)[0:-4] + 'T' + row[0]) \
                + 1/2*(dt.datetime.fromisoformat(ntpath.basename(filename)[0:-4] + 'T' + row[1]) \
                - dt.datetime.fromisoformat(ntpath.basename(filename)[0:-4] + 'T' + row[0]))
            for key in ['Frecuencia cardiaca media (ppm)', 'Frecuencia cardiaca máxima (ppm)', 'Frecuencia cardíaca mínima (ppm)']:
                if rowdict.get(key) in ['', None]:
                    rowdict[key] = None
                else:
                    rowdict[key] = float(rowdict[key])
            heartdict['Value'] = rowdict['Frecuencia cardiaca media (ppm)']
            heartdict['Max'] = rowdict['Frecuencia cardiaca máxima (ppm)']
            heartdict['Min'] = rowdict['Frecuencia cardíaca mínima (ppm)']
            if heartdict['Value'] != None and heartdict['Value'] != '':
                heartitem = Data(Type='HeartRate', Time=heartdict['Time'], Value=heartdict['Value'])
                if heartdict['Min'] and heartdict['Max']:
                    heartitem.Min = float(heartdict['Min'])
                    heartitem.Max = float(heartdict['Max'])
                heartlist.append(heartitem)
        csvfile.close()
        return heartlist

    # Filters for cleaning the data.

    @staticmethod
    def cleantrack(track):
        # Remove duplicates.
        track = [ dict(t) for t in {tuple(d.items()) for d in track}]
        # When the times coincide in a track, prefer longer distances.
        unique = set( item['Time'] for item in track )
        unique = { time: [] for time in unique }
        for item in track:
            unique[item['Time']].append(item['Distance'])
        unique = ( (key, max(values)) for key, values in unique.items() )
        track = [ item for item in track if (item['Time'], item['Distance']) in unique]
        # Prefer times with location data over items without it.
        newtrack = [] # We start with an empty track.
        for id in unique:
            duplicates = [ item for item in track if (item['Time'], item['Distance']) == id ]
            if len(duplicates) == 1:
                finalitem = duplicates[0]
            else:
                finalitem = [ item for item in duplicates if item.get('Latitude') and item.get('Longitude') and item.get('Altitude') ]
                finalitem = finalitem[0]
            newtrack.append(finalitem)
        return newtrack

    @staticmethod
    def filteractivity(activity):
        if activity.Type == 'Other':
            activity.Type = activity.Notes
        filtered = False
        # Filter by duration.
        if activity.Duration < dt.timedelta(minutes=4):
            filtered = True
        # Filter out too slow and fast activities.
        if activity.Type == 'Walking'or activity.Type == 'Running':
            speed = activity.Distance / activity.Duration.total_seconds()
            if (speed >= 2 or speed < 0.35) and activity.Type == 'Walking' and activity.Distance < 200:
                filtered = True
            if (speed >= 11 or speed < 2.1) and activity.Type == 'Running' and activity.Distance < 200:
                filtered = True
            if speed < 0.1:
                filtered = True
        if filtered:
            return None
        else:
            return activity

#####################
##### TCX files #####
#####################
'''This code is here for possible future use, but it is not production code.
It was meant to work alongside the Gogle Fit code.'''

def readtcx(filename):
    """Returns a list with all the activities contained in a TCX file.
    Each lap in an activity with multiple laps is converted to
    a single activity."""
    activitylist = []
    tree = removenamespaces(et.parse(filename))
    root = tree.getroot()
    activities = getfields(root, 'Activities', unique=True)
    activities = getfields(activities, 'Activity', unique=False)
    if activities:
        for activity in activities:
            activityid = getfields(activity, 'Id', unique=True).text
            activitytype = activity.attrib['Sport']
            activitynotes = getfields(activity, 'Notes', unique=True).text
            laps = getfields(activity, 'Lap', unique=False)
            lapcount = 0
            for lap in laps:
                # Get lap general information.
                lapcount += 1
                cact = Activity(activityid + '_' + str(lapcount), activitytype)
                cact.StartTime = dt.datetime.fromisoformat(lap.attrib['StartTime'].replace('Z', '+00:00'))
                cact.EndTime = cact.StartTime + dt.timedelta(seconds=float(getfields(lap, 'TotalTimeSeconds', unique=True, content=True)))
                cact.Distance = float(getfields(lap, 'DistanceMeters', unique=True, content=True))
                cact.Calories = float(getfields(lap, 'Calories', unique=True, content=True))
                cact.Trigger = getfields(lap, 'TriggerMethod', unique=True, content=True)
                cact.Intensity = getfields(lap, 'Intensity', unique=True, content=True)
                cact.Notes = activitynotes
                # Get track information.
                track = getfields(lap, 'Track', unique=True)
                trackpoints = getfields(track, 'Trackpoint', unique = False)
                for trackpoint in trackpoints:
                    ctrackp = { 'Time':             dt.datetime.fromisoformat(getfields(trackpoint, 'Time', unique=True, content=True).replace('Z', '+00:00')),
                                'Distance':   getfields(trackpoint, 'DistanceMeters', unique=True, content=True),
                                'Latitude':  getfields(getfields(trackpoint, 'Position', unique=True), 'LatitudeDegrees', unique=True, content=True),
                                'Longitude': getfields(getfields(trackpoint, 'Position', unique=True), 'LongitudeDegrees', unique=True, content=True),
                                'Altitude':   getfields(trackpoint, 'AltitudeMeters', unique=True, content=True),
                                }
                    ctrackp = { key: item for key,item in ctrackp.items() if item}
                    for key in ctrackp:
                        if key in ['Distance','Latitude','Longitude','Altitude']:
                            ctrackp[key] = float(ctrackp[key])
                            cact.Track.append(ctrackp)
                            # Append the lap to the activity list.
                activitylist.append(cact)
    return activitylist

def removenamespaces(tree):
    for item in tree.iter():
        item.tag = item.tag.rpartition('}')[2]
    return tree

def getfields(element, field, unique=False, content=False):
    """Returns a list with all the children of 'element' tagged as 'field'.

    When the argument 'unique' is set to 'True', the function will assume that there is only one child tagged as 'field' an return such child directly instead of embedding it into a list.

    When the argument 'content' is set to 'True', the text content of the children will be returned instead of the child itself.
    """
    if element == None:
        return None
    if content == False:
        fields = [ item for item in element if item.tag == field]
    else:
        fields = [ item.text for item in element if item.tag == field]
    if len(fields) == 0:
        #raise Exception('Field not found.')
        return None
    if unique == True and len(fields) > 1:
        #raise Exception('Nonunique field.')
        return None
    elif unique == False:
        return fields
    else:
        return fields[0]

